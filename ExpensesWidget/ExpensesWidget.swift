//
//  ExpensesWidget.swift
//  ExpensesWidget
//
//  Created by Mayank Vamja on 25/06/21.
//

import WidgetKit
import SwiftUI
import Intents
import CoreData

struct Provider: IntentTimelineProvider {
    
    func getRequest(by configuration: ConfigurationIntent) -> NSFetchRequest<Transactions> {
        return Transactions.fetchTransactions(filterTime: .month)
    }
    
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), configuration: ConfigurationIntent(), request: getRequest(by: ConfigurationIntent()))
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> Void) {
        let entry = SimpleEntry(date: Date(), configuration: configuration, request: getRequest(by: configuration))
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> Void) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate, configuration: configuration, request: getRequest(by: configuration))
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let configuration: ConfigurationIntent
    let request: NSFetchRequest<Transactions>
}

struct DashboardCard: View {
    let image: Image
    let radius: CGFloat
    let color: Color
    let title: String
    let subtitle: String
    
    var body: some View {
        VStack {
            
            image
                .resizable()
                .frame(width: 20, height: 20)
                .padding()
                .foregroundColor(color)
                .background(Circle().fill(color.opacity(0.2)))
            
            Text(title)
                .font(.title3)
                .fontWeight(.bold)
            
            Text(subtitle)
                .font(.title3)
                .fontWeight(.semibold)
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(radius)
        .overlay(RoundedRectangle(cornerRadius: radius)
                    .stroke(color, lineWidth: 2))
    }
}

struct ExpensesWidgetEntryView: View {
    var entry: Provider.Entry
    var transactions: FetchRequest<Transactions>
    var data: FetchedResults<Transactions> {
        transactions.wrappedValue
    }
    var text: String = "-"
    
    init(entry: Provider.Entry) {
        self.entry = entry
        
        let context = PersistanceController.transactions.context
        do {
            let results = try context.fetch(entry.request)
            text = "\(results.expenses.count) + \(results.count) + \(results.expenses.count)"
        }
        catch {
            text = "\((error as NSError).userInfo)"
        }
        
        self.transactions = FetchRequest(fetchRequest: entry.request)
    }

    var body: some View {
//        VStack {
//            Text(text)
            DashboardCard(image: Image(systemName: "arrow.up"), radius: 20, color: .red, title: "Expenses", subtitle: "\(data.expenses.sum)")
//        }
    }
}

// @main
struct ExpensesWidget: Widget {
    let kind: String = "ExpensesWidget"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            ExpensesWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Expenses")
        .description("Expenses of the month")
        .supportedFamilies([.systemSmall])
    }
}

struct ExpensesWidget_Previews: PreviewProvider {
    static var previews: some View {
        ExpensesWidgetEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent(), request: Transactions.fetchTransactions()))
            .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
