//
//  INewsWidget.swift
//  iNews
//
//  Created by Mayank Vamja on 29/06/21.
//

import SwiftUI
import WidgetKit
import Combine

struct INewsProvider: TimelineProvider {
        
    func placeholder(in context: Context) -> INewsEntry {
        
        return INewsEntry(date: Date(), topics: [])
    }
    
    func getSnapshot(in context: Context, completion: @escaping (INewsEntry) -> Void) {
        var dataProvider = INewsWidgetDataProvider()
        dataProvider.fetchData { (entry) in
            completion(entry)
        }
    }
    
    func getTimeline(in context: Context, completion: @escaping (Timeline<INewsEntry>) -> Void) {
        var dataProvider = INewsWidgetDataProvider()
        dataProvider.fetchData { (entry) in
            let timeline = Timeline<INewsEntry>(entries: [entry], policy: .atEnd)
            completion(timeline)
        }
    }
}

struct INewsWidgetDataProvider {
    private var cancellable: AnyCancellable? = nil

    mutating func fetchData(date: Date = Date(), completion: @escaping (INewsEntry) -> Void) {
        cancellable =  HTTPManager.shared.request(.init(Endpoints.NewsAPI.trendingTopics)).receive(on: DispatchQueue.main)
            .sink { (result) in
                switch result {
                case .finished:
                    break
                case .failure:
                    completion(.init(date: date, topics: []))
                }
            } receiveValue: { (data: TrendingNewsTopics) in
                completion(.init(date: date, topics: data.topics))
            }
    }
}

struct INewsEntry: TimelineEntry {
    let date: Date
    let topics: [TrendingTopic]
}

struct INewsWidgetEntryView: View {
    var entry: INewsProvider.Entry
    @State private var totalHeight: CGFloat = .zero
    
    var body: some View {
        VStack {
            Text("TOPICS: \(entry.topics.count)")
            VStack {
                GeometryReader { geometry in
                    self.generateContent(in: geometry)
                }
            }.frame(height: totalHeight)
        }
    }
    
    private func generateContent(in geometry: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(entry.topics, id: \.self) { item in
                self.item(for: item)
                    .padding(4)
                    .alignmentGuide(.leading, computeValue: { itemSize in
                        if abs(width - itemSize.width) > geometry.size.width {
                            width = 0
                            height -= itemSize.height
                        }
                        let result = width
                        if item == entry.topics.last! {
                            width = 0
                        }
                        else {
                            width -= itemSize.width
                        }
                        return result
                    })
                    .alignmentGuide(.top, computeValue: {_ in
                        let result = height
                        if item == entry.topics.last! {
                            height = 0
                        }
                        return result
                    })
            }
        }.background(viewHeightReader($totalHeight))
    }
    
    private func item(for item: TrendingTopic) -> some View {
        Text(item.query ?? "-#-")
            .font(.title3)
            .fontWeight(.bold)
            .padding()
            .background(Color.white)
            .cornerRadius(4)
            .shadow(radius: 4)
    }
    
    private func viewHeightReader(_ binding: Binding<CGFloat>) -> some View {
        return GeometryReader { geometry -> Color in
            let rect = geometry.frame(in: .local)
            DispatchQueue.main.async {
                binding.wrappedValue = rect.size.height
            }
            return .clear
        }
    }
}

struct INewsWidget: Widget {
    private let kind = "INewsWidget"
    
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: INewsProvider()) { (entry) in
            INewsWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Trending Topics")
        .description("See all trending news topics")
        .supportedFamilies([.systemLarge])
    }
    
}

@main
struct INewsWidgetBundle: WidgetBundle {
    
    @WidgetBundleBuilder
    var body: some Widget {
        INewsWidget()
        ExpensesWidget()
    }
}
