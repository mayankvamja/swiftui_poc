//
//  Color+Extension.swift
//  iNews
//
//  Created by Mayank Vamja on 08/05/21.
//

import SwiftUI

extension Color {
    init(_ red: Int, _ green: Int, _ blue: Int, _ alpha: Double = 1) {
        self.init(.sRGB, red: Double(red) / 255, green: Double(green) / 255, blue: Double(blue) / 255, opacity: alpha)
    }
    
    init(_ hex: UInt, _ alpha: Double = 1) {
        self.init(
            .sRGB,
            red: Double((hex >> 16) & 0xff) / 255,
            green: Double((hex >> 08) & 0xff) / 255,
            blue: Double((hex >> 00) & 0xff) / 255,
            opacity: alpha
        )
    }
    
    public static let primaryColor = Color(0x00b0ff)
    public static let secondaryColor = Color(0x4d8af0)
    public static let contrastColor = Color(0x3ad29f)
    public static let backgroundColor = Color("background")
    public static let darkColor = Color(0x212121)
    public static let lightColor = Color(0xeaeaea)
    public static let shadowColor = Color.gray.opacity(0.3)
}
