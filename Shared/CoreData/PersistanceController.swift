//
//  PersistanceController.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import CoreData

struct PersistanceController {
    
    static let transactions = PersistanceController("transactions")
    
    let container: NSPersistentContainer
    
    var context: NSManagedObjectContext {
        container.viewContext
    }
    
    init(_ xcdatamodelName: String) {
        container = NSPersistentContainer(name: "transactions")
        
        container.loadPersistentStores { (description, error) in
            if let _error = error {
                fatalError("Failed to load \(description.description) with error: \(_error)")
            }
        }
    }
}
