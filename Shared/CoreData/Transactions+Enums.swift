//
//  Transactions+Enums.swift
//  iNews
//
//  Created by Mayank Vamja on 16/06/21.
//

import Foundation

extension String {
    var localized: String {
        // : Implement localized string from here
        return self
    }
}

enum TransactionType: String, CaseIterable, Hashable, CustomStringConvertible {
    case expense = "Expense"
    case income = "Income"
    
    var description: String { self.rawValue.localized }
}

enum IncomeTag: String, CaseIterable, CustomStringConvertible {
    case Sallary = "Sallary"
    case PartTimeJob = "Part time work"
    case Bonus = "Bonus Income"
    case Personal = "Personal"
    case InvestmentReturn = "Returns from investments"
    case none = "None"
    
    var description: String { self.rawValue.localized }
}

enum TransactionMode: String, CaseIterable, CustomStringConvertible {
    case creditCard = "Credit Card"
    case debitCard = "Debit Card"
    case cash = "Cash Transaction"
    case UPI = "UPI"
    case netBanking = "Net banking"
    case bank = "Bank"
    case none = "None"
    
    var description: String { self.rawValue.localized }
}

enum ExpenseTag: String, CaseIterable, CustomStringConvertible {
    case Entertainement
    case Health
    case Households
    case Saving
    case Personal
    case Food
    case EMI
    case Insurance
    case Utilities
    case Other
    
    var description: String { self.rawValue.localized }
}

extension TransactionType {
    
}

extension IncomeTag {
    var imageName: String {
        switch self {
        case .Sallary:
            return "banknote.fill"
        case .PartTimeJob:
            return "clock"
        case .InvestmentReturn:
            return "return"
        case .none:
            return "diamond"
        case .Bonus:
            return "gift"
        case .Personal:
            return "lock.fill"
        }
    }
    
    var color: UInt {
        switch self {
        case .Sallary:
            return 0xFC427B
        case .PartTimeJob:
            return 0x3B3B98
        case .InvestmentReturn:
            return 0x58B19F
        case .none:
            return 0x2C3A47
        case .Bonus:
            return 0x58B19F
        case .Personal:
            return 0xB33771
        }
    }
    
}

extension ExpenseTag {
    var imageName: String {
        switch self {
        case .Entertainement:
            return "tv"
        case .Health:
            return "waveform.path.ecg"
        case .Households:
            return "house"
        case .Saving:
            return "banknote.fill"
        case .Personal:
            return "lock.fill"
        case .Food:
            return "heart"
        case .EMI:
            return "coloncurrencysign.circle"
        case .Insurance:
            return "note.text"
        case .Utilities:
            return "gearshape.2"
        case .Other:
            return "diamond"
        }
    }
    
    var color: UInt {
        switch self {
        case .Entertainement:
            return 0xF97F51
        case .Health:
            return 0xFC427B
        case .Households:
            return 0x82589F
        case .Saving:
            return 0xEAB543
        case .Personal:
            return 0xB33771
        case .Food:
            return 0x58B19F
        case .EMI:
            return 0x3B3B98
        case .Insurance:
            return 0xFD7272
        case .Utilities:
            return 0x25CCF7
        case .Other:
            return 0x2C3A47
        }
    }
}

enum ExpenseSort: String, Hashable, CaseIterable {
    case createdAt = "Date added"
    case updatedAt = "Date modified"
    case transactionDate = "Ocurred on"
    
    var key: String {
        switch self {
        case .createdAt:
            return "createdAt"
        case .updatedAt:
            return "updatedAt"
        case .transactionDate:
            return "transactionDate"
        }
    }
}

enum ExpenseFilterTime: String, Hashable, CaseIterable {
    case all = "All time"
    case week = "This week"
    case month = "This month"
}

extension ExpenseFilterTime {
    var filteredDates: (startDate: NSDate, endDate: NSDate)? {
        let endDate: NSDate = Date() as NSDate
        switch self {
        
        case .all:
            return nil
        case .week:
            let startDate: NSDate = Date().lastWeek as NSDate
            return (startDate: startDate, endDate: endDate)
            
        case .month:
            let startDate: NSDate = Date().lastMonth as NSDate
            return (startDate: startDate, endDate: endDate)
            
        }
    }
}
