//
//  Transcations.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import CoreData

extension Transactions {
    
    static func fetchTransactions(
        _ type: TransactionType? = nil,
        sortBy: ExpenseSort = .transactionDate,
        ascending: Bool = true,
        filterTime: ExpenseFilterTime = .all,
        fetchLimit: Int? = nil,
        filterText: String? = nil,
        filterTag: String? = nil
    ) -> NSFetchRequest<Transactions> {
        
        let request: NSFetchRequest<Transactions> = Transactions.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: sortBy.key, ascending: ascending)
        var predicates: [NSPredicate] = []
        
        if let transactionType = type?.rawValue {
            predicates.append(NSPredicate(format: "transactionType MATCHES %@", transactionType))
        }
        if let (startDate, endDate) = filterTime.filteredDates {
            predicates.append(NSPredicate(format: "transactionDate >= %@ AND transactionDate <= %@", startDate, endDate))
        }
        if let text = filterText?.trimmingCharacters(in: .whitespacesAndNewlines), text.count > 0 {
            predicates.append(NSPredicate(format: "title CONTAINS[c] %@", text))
        }
        if let tag = filterTag, tag.count > 0 {
            predicates.append(NSPredicate(format: "tag MATCHES %@", tag))
        }
        if let _limit = fetchLimit {
            request.fetchLimit = _limit
        }
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        request.sortDescriptors = [sortDescriptor]
        
        return request
    }
    
    static func fetchTransactions(
        _ type: TransactionType? = nil,
        sortBy: ExpenseSort = .transactionDate,
        ascending: Bool = true,
        filterTime: ExpenseFilterTime = .all,
        fetchLimit: Int? = nil,
        filterText: String? = nil,
        filterTag: String? = nil,
        context: NSManagedObjectContext,
        completion: @escaping ([Transactions]) -> Void
    ) {
        
        let request: NSFetchRequest<Transactions> = Transactions.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: sortBy.key, ascending: ascending)
        var predicates: [NSPredicate] = []
        
        if let transactionType = type?.rawValue {
            predicates.append(NSPredicate(format: "transactionType MATCHES %@", transactionType))
        }
        if let (startDate, endDate) = filterTime.filteredDates {
            predicates.append(NSPredicate(format: "transactionDate >= %@ AND transactionDate <= %@", startDate, endDate))
        }
        if let text = filterText?.trimmingCharacters(in: .whitespacesAndNewlines), text.count > 0 {
            predicates.append(NSPredicate(format: "title CONTAINS[c] %@", text))
        }
        if let tag = filterTag, tag.count > 0 {
            predicates.append(NSPredicate(format: "tag MATCHES %@", tag))
        }
        if let _limit = fetchLimit {
            request.fetchLimit = _limit
        }
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        request.sortDescriptors = [sortDescriptor]
        
        context.perform {
            do {
                let results = try request.execute()
                completion(results)
            }
            catch {
                dump(error)
                completion([])
            }
        }
    }
    
    static func fetchChartData(_ type: TransactionType = .expense, context: NSManagedObjectContext, by filterTime: ExpenseFilterTime, completion: @escaping ([(sum: Double, tag: String)]) -> Void) {
        
        let keypathExp1 = NSExpression(forKeyPath: "amount")
        let expression = NSExpression(forFunction: "sum:", arguments: [keypathExp1])
        let sumDesc = NSExpressionDescription()
        sumDesc.expression = expression
        sumDesc.name = "value"
        sumDesc.expressionResultType = .integer64AttributeType
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: Transactions.entity().name ?? "Transactions")
        request.returnsObjectsAsFaults = false
        request.propertiesToGroupBy = ["tag"]
        request.propertiesToFetch = ["tag", sumDesc]
        request.resultType = .dictionaryResultType
        
        var predicates: [NSPredicate] = []
        predicates.append(NSPredicate(format: "transactionType MATCHES %@", type.rawValue))
        if let (startDate, endDate) = filterTime.filteredDates {
            predicates.append(NSPredicate(format: "transactionDate >= %@ AND transactionDate <= %@", startDate, endDate))
        }

        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)

        context.perform {
            do {
                let results = try request.execute()
                let data = results.map { (result) -> (Double, String)? in
                    guard
                        let resultDict = result as? [String: Any],
                        let amount = resultDict["value"] as? Double, amount > 0,
                        let tag = resultDict["tag"] as? String else {
                        return nil
                    }
                    return (amount, tag)
                }.compactMap { $0 }
                completion(data)
            } catch let error as NSError {
                dump(error)
                completion([])
            }
        }
    }
    
}

extension Collection where Element == Transactions {
    
    var expenses: [Transactions] {
        self.filter({$0.transactionType == "Expense"})
    }
    
    var incomes: [Transactions] {
        self.filter({$0.transactionType == "Income"})
    }
    
    var sum: Double {
        self.reduce(0.0) { (res, item) -> Double in
            res + item.amount
        }
    }
    
    var max: Double {
        self.compactMap({$0.amount}).max() ?? 0.0
    }
    
    var min: Double {
        self.compactMap({$0.amount}).min() ?? 0.0
    }
    
}
