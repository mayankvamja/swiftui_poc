//
//  Date+Extension.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import Foundation

extension Date {
    init?(from utc: String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        guard let date = formatter.date(from: utc) else {
            return nil
        }
        
        self = date
    }
    
    func formated(format: String = "MMM dd, yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var humanReadable: String {
        if self.timeIntervalSinceNow <= 5 && self.timeIntervalSinceNow >= -5 {
            return "Just now"
        }
        else if self.timeIntervalSinceNow > 5 {
            let seconds = Int(self.timeIntervalSinceNow)
            guard seconds > 60 else { return "\(seconds) seconds after" }
            
            let minutes = seconds / 60
            guard minutes > 60 else { return "\(minutes) mins after" }
            
            let hours = minutes / 60
            guard hours > 24 else { return "\(hours) hours after" }
            
            let days = hours / 24
            guard days > 7 else { return "\(days) days after" }
            
            let weeks = days / 7
            guard weeks > 52 else { return "\(weeks) weeks after" }
            
            let years = weeks / 52
            return "\(years) years after"
        }
        else {
            let seconds = Int(-self.timeIntervalSinceNow)
            guard seconds > 60 else { return "\(seconds) seconds ago" }
            
            let minutes = seconds / 60
            guard minutes > 60 else { return "\(minutes) mins ago" }
            
            let hours = minutes / 60
            guard hours > 24 else { return "\(hours) hours ago" }
            
            let days = hours / 24
            guard days > 7 else { return "\(days) days ago" }
            
            let weeks = days / 7
            guard weeks > 52 else { return "\(weeks) weeks ago" }
            
            let years = weeks / 52
            return "\(years) years ago"
        }
    }
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    
    var lastWeek: Date {
        return Calendar.current.date(byAdding: .day, value: -7, to: self)!
    }
    
    var lastMonth: Date {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)!
    }
    
    var last3Month: Date {
        return Calendar.current.date(byAdding: .month, value: -3, to: self)!
    }
    
    var last6Month: Date {
        return Calendar.current.date(byAdding: .month, value: -6, to: self)!
    }
    
    static func mondayAt12AM() -> Date {
        return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
    }
}
