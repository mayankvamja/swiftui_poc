//
//  News+Models.swift
//  iNews
//
//  Created by Mayank Vamja on 08/05/21.
//

import Foundation

enum NewsCategory: String {
    case topHeadlines = "top-headlines"
    case business
    case entertainment
    case general
    case health
    case science
    case sports
    case technology
}

// MARK: - NewsData
struct NewsData: Decodable {
    let totalResults: Int
    let articles: [NewsArticle]
    let page, pagesize: Int
}

// MARK: - NewsArticle
struct NewsArticle: Hashable, Decodable {
    let id = UUID()
    let title: String?
    let content: String?
    let source: String?
    let timeAgo: String?
    let url: String?
    let imageURL: String?

    enum CodingKeys: String, CodingKey {
        case title, content, timeAgo, source, url
        case imageURL = "imageUrl"
    }
}
