//
//  OnBoardingModel.swift
//  iNews
//
//  Created by Mayank Vamja on 08/05/21.
//

import Foundation
import SwiftUI

struct OnBoardingModel: Equatable, Identifiable {
    static func == (lhs: OnBoardingModel, rhs: OnBoardingModel) -> Bool {
        lhs.id == rhs.id
    }
 
    let id = UUID()
    let title: String
    let content: String
    let image: String
    let colors: [Color]
}
