//
//  TrendingNewsTopics.swift
//  iNews
//
//  Created by Mayank Vamja on 08/05/21.
//

import Foundation

// MARK: TopicKeyword
struct TopicKeyword: Decodable, Hashable {
    let mid, title, type: String
}

// MARK: - TrendingNewsTopics
struct TrendingNewsTopics: Decodable {
    let date: String?
    let formattedDate: String?
    let topics: [TrendingTopic]
    
    enum CodingKeys: String, CodingKey {
        case date, formattedDate
        case topics = "trendingSearches"
    }
}

// MARK: - TrendingTopic
struct TrendingTopic: Decodable, Identifiable, Hashable {
    static func == (lhs: TrendingTopic, rhs: TrendingTopic) -> Bool {
        lhs.id == rhs.id
    }
    
    let id = UUID()
    let query: String?
    let formattedTraffic: String?
    let relatedQueries: [String]
    let imageURL: String?
    let articles: [NewsArticle]

    enum CodingKeys: String, CodingKey {
        case query, formattedTraffic, relatedQueries
        case imageURL = "imageUrl"
        case articles
    }
}
