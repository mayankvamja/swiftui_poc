//
//  Endpoints.swift
//  iNews
//
//  Created by Mayank Vamja on 31/05/21.
//

import Foundation

/// Endpoint with `path` to `EndpointHost`
public protocol Endpoint {
    var path: String { get }
}

public typealias EndpointHost = String

struct Endpoints {
    
    enum NewsAPI: Endpoint {
        static let host: EndpointHost = "newsoasis.herokuapp.com"
        
        case topicsSearch
        case trendingTopics
        case trendingToday
        case newsByCategory(_ category: NewsCategory)
        case searchNews
        case sources
        case artical
        
        var path: String {
            switch self {
            case .topicsSearch:
                return "/api/news/topics-search"
            case .trendingTopics:
                return "/api/news/trending-topics"
            case .trendingToday:
                return "/api/news/trending-today"
            case .newsByCategory(let cat):
                return "/api/news/\(cat.rawValue)"
            case .searchNews:
                return "/api/news/everything"
            case .sources:
                return "/api/news/sources"
            case .artical:
                return "/api/news/artical"
            }
        }
        
    }
}
