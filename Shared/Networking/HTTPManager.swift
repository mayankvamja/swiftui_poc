//
//  Requestable.swift
//  iNews
//
//  Created by Mayank Vamja on 31/05/21.
//

import Combine
import Foundation

/// enum for HTTP Method to use in http request
public enum HTTPMethod: String {
    case GET
    case POST
    case PATCH
    case PUT
    case DELETE
}

/// Error type enum for Http Requests and parsing
public enum HTTPError: Error, Equatable, LocalizedError {
    /// api error with Http Url Response code and error message
    case apiError(code: Int, error: String)
    
    /// bad url
    case badRequest(_ error: String)
    
    /// server error
    case serverError(code: Int, error: String)
    
    /// nil response
    case noResponse(_ error: String)
    
    /// parsing error
    case unableToParseData(_ error: String)
    
    /// unknown error
    case unknown(_ error: String)
    
    public var errorDescription: String? {
        switch self {
        case .apiError(_, let msg):
            return msg
        
        case .badRequest(let msg):
            return msg

        case .serverError(_, let msg):
            return msg

        case .noResponse(let msg):
            return msg

        case .unableToParseData(let msg):
            return msg

        case .unknown(let msg):
            return msg

        }
    }
}

/// Protocol for HTTPRequest
public protocol HTTPRequestProtoccol {
    var host: EndpointHost { get }
    var cachePolicy: URLRequest.CachePolicy { get }
    var timeout: TimeInterval { get }

    var method: HTTPMethod { get }
    var path: Endpoint { get }
    var parameters: [String: String]? { get }
    var body: Encodable? { get }
    var headers: [String: String]? { get }
    var urlRequest: URLRequest? { get }
}

/// struct for HTTPRequest
struct HTTPRequest: HTTPRequestProtoccol {
    static var defaultHost: EndpointHost = ""
    static var defaultHeaders: [String: String]? = nil
    
    var cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
    
    var method: HTTPMethod
    var host: EndpointHost
    var path: Endpoint
    var parameters: [String: String]?
    var body: Encodable?
    var urlRequest: URLRequest?
    var headers: [String: String]?
    var timeout: TimeInterval = 300.0
    
    /// init w/o http body
    init(
        _ path: Endpoint,
        host: EndpointHost = HTTPRequest.defaultHost,
        method: HTTPMethod = .GET,
        parameters: [String: String]? = nil,
        timeout: TimeInterval = 300.0,
        headers: [String: String]? = HTTPRequest.defaultHeaders
    ) {
        self.host = host
        self.path = path
        self.method = method
        self.parameters = parameters
        self.body = nil
        
        var component = URLComponents()
        component.scheme = "https"
        component.host = host
        component.path = path.path
        component.queryItems = parameters?.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = component.url else { return }
        var request  = URLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: timeout)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        
        self.urlRequest = request
    }
    
    /// init with `Encodable` http body type
    init<T: Encodable>(
        _ path: Endpoint,
        host: EndpointHost = HTTPRequest.defaultHost,
        method: HTTPMethod = .GET,
        parameters: [String: String]? = nil,
        body: T? = nil,
        timeout: TimeInterval = 300.0,
        headers: [String: String]? = nil
    ) {
        self.host = host
        self.path = path
        self.method = method
        self.parameters = parameters
        self.body = body

        var component = URLComponents()
        component.host = host
        component.path = path.path
        component.queryItems = parameters?.map { URLQueryItem(name: $0, value: $1) }
        
        guard let url = component.url else { return }
        var request  = URLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: timeout)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = headers
        request.httpBody = try? JSONEncoder().encode(body)
        
        self.urlRequest = request
    }
    
    /// init with [String: Any] body type
    /* init(...) {
     
     } */
    
    /// prse request to `Publisher`
    /// uses shared default `HTTPManager`
    func parse<T: Decodable>() -> AnyPublisher<T, HTTPError> {
        return HTTPManager.shared.request(self)
    }
    
}

/// protocol for Network Manager to parse http requests
protocol HTTPManagerProtocol {
    func request<T: Decodable>(_ req: HTTPRequest) -> AnyPublisher<T, HTTPError>
}

struct HTTPManager: HTTPManagerProtocol {
    
    static let shared: HTTPManager = HTTPManager(.shared)
    
    private let session: URLSession
    
    init(_ session: URLSession) {
        self.session = session
    }
    
    init(_ configuration: URLSessionConfiguration = .default) {
        session = URLSession(configuration: configuration)
    }
    
    func request<T: Decodable>(_ req: HTTPRequest) -> AnyPublisher<T, HTTPError> {

        session.configuration.timeoutIntervalForRequest = req.timeout

        guard let urlRequest = req.urlRequest else {
            return AnyPublisher(
                Fail<T, HTTPError>(error: HTTPError.badRequest("Check Url or parameters."))
            )
        }
                
        return session
            .dataTaskPublisher(for: urlRequest)
            .tryMap { output in
                guard let urlResponse = output.response as? HTTPURLResponse else {
                    throw HTTPError.noResponse("No response from Server")
                }
                
                switch urlResponse.statusCode {
                case 0...199, 300...399:
                    throw HTTPError.apiError(code: urlResponse.statusCode, error: "Unexpected response from server")
                
                case 200...299:
                    return output.data
                
                case 400...499:
                    throw HTTPError.apiError(code: urlResponse.statusCode, error: "Check for \(urlResponse.statusCode) code.")
                
                case 500...:
                    throw HTTPError.serverError(code: urlResponse.statusCode, error: "\(urlResponse.statusCode) Server Error.")
                
                default:
                    return output.data
                }
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                return HTTPError.unknown(error.localizedDescription)
            }
            .eraseToAnyPublisher()
    }
    
}

extension Dictionary {
    mutating func merge(_ dict: [Key: Value]) -> Dictionary {
        for (key, value) in dict {
            updateValue(value, forKey: key)
        }
        return self
    }
}
