//
//  BarChart.swift
//  iNews
//
//  Created by Mayank Vamja on 21/06/21.
//

import SwiftUI
import Combine

class BarChartModel: ObservableObject {
    @Published var selectedBar: BarChart.Item? = nil
    @Binding var data: [BarChart.Item]
    var maxValue: Double = 0.0
    var title: String = ""
    
    init(_ title: String, _ data: Binding<[BarChart.Item]>) {
        self.title = title
        self._data = data
        self.maxValue = abs(self.data.map({$0.value}).max() ?? 0)
        self.selectedBar = self.data.count > 0 ? self.data[0] : nil
        
        print(self.maxValue)
        print(self.selectedBar)
    }
}

/// Bar chart view
struct BarChart: View {
    
    @ObservedObject private var model: BarChartModel
    @State private var barAppearing = false
    private let shouldShowXAxisLabel: Bool
    
    init(_ title: String, _ data: Binding<[Item]>, shouldShowXAxisLabel: Bool = false) {
        _model = ObservedObject(wrappedValue: BarChartModel(title, data))
        self.shouldShowXAxisLabel = shouldShowXAxisLabel
    }
    
    /*init(_ title: String, _ data: [(sum: Double, tag: String)]) {
     let itemData = data.map({ (item) -> Item in
     let color = Color(ExpenseTag(rawValue: item.tag)?.color ?? IncomeTag(rawValue: item.tag)?.color ?? 0x545454)
     let icon = ExpenseTag(rawValue: item.tag)?.imageName ?? IncomeTag(rawValue: item.tag)?.imageName ?? "info.circle"
     let label = Label(item.tag, systemImage: icon)
     return Item(item.sum, label: label, color: color)
     })
     self.init(title, Binding(get: { itemData }, set: { _ in }))
     }*/
    
    private func barHeight(from value: Double, and boundsHeight: CGFloat) -> CGFloat {
        model.maxValue == 0 ? 1 : CGFloat(Double(boundsHeight - 150) * value / model.maxValue)
    }
    
    private func barWidth(bounds width: CGFloat) -> CGFloat {
        let calucatedWidth = (width-CGFloat(model.data.count*9))/CGFloat(model.data.count)
        return min(50, calucatedWidth)
    }
    
    /// BarChart Item struct
    struct Item: Equatable, Identifiable {
        static func == (lhs: BarChart.Item, rhs: BarChart.Item) -> Bool {
            lhs.id == rhs.id
        }
        
        let id: UUID
        let value: Double
        let label: AnyView
        let color: Color
        
        init<Content: View>(_ value: Double, label: Content, color: Color = .primaryColor) {
            self.id = UUID()
            self.value = value
            self.label = AnyView(label)
            self.color = color
        }
        
        init(_ value: Double, label: String = "", color: Color = .primaryColor) {
            self.id = UUID()
            self.value = value
            self.label = AnyView(Text(label))
            self.color = color
        }
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                VStack(alignment: .leading) {
                    VStack(alignment: .leading) {
                        Text(model.title)
                            .font(.title)
                            .fontWeight(.semibold)
                        
                        HStack(alignment: .bottom, spacing: 2) {
                            ForEach(model.data) { item in
                                VStack {
                                    Spacer()
                                    Text(String(format: "%.1f", item.value))
                                        .font(.caption)
                                    
                                    RoundedRectangle(cornerRadius: 20)
                                        .foregroundColor(item.color)
                                        .frame(
                                            width: barWidth(bounds: geometry.size.width),
                                            height: barAppearing ? barHeight(from: item.value, and: geometry.size.height) : 0
                                        )
                                        .padding(3)
                                        
                                        .overlay(RoundedRectangle(cornerRadius: 20).stroke(item.color, lineWidth: item != model.selectedBar ? 0 : 2))
                                    
                                    if shouldShowXAxisLabel {
                                        item.label
                                            .foregroundColor(.gray)
                                            .font(.caption)
                                    }
                                }
                                .onTapGesture {
                                    model.selectedBar = item
                                }
                                .animation(Animation.easeInOut(duration: 0.5))
                            }
                        }
                        
                        VStack {
                            model.selectedBar?.label
                                .foregroundColor(model.selectedBar?.color ?? .black)
                                .font(.system(size: 22, weight: .semibold, design: .rounded))
                            Text(String(format: "%.2f", model.selectedBar?.value ?? 0)).font(.body)
                        }.padding(.horizontal, 10)
                        
                    }
                    .onAppear {
                        withAnimation {
                            barAppearing = true
                        }
                    }
                    .onChange(of: model.data) { _ in
                        barAppearing = false
                        withAnimation {
                            barAppearing = true
                        }
                    }
                }
                .frame(maxWidth: geometry.size.width, maxHeight: geometry.size.height)
                
            }
        }
    }
}

struct BarChart_Previews: PreviewProvider {
    static var previews: some View {
        BarChart("Bar Chart",
                 .constant(
                    [
                        .init(20, label: Text("Hello"), color: .red),
                        .init(10, label: Text("There"), color: .blue),
                        .init(4, label: Text("World"), color: .green)
                    ]
                 )
        )
        .frame(maxHeight: 200)
    }
}
