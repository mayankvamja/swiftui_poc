//
//  CustomTextField.swift
//  iNews
//
//  Created by Mayank Vamja on 24/06/21.
//

import SwiftUI

/// TextField with binding of firstresponder
// TODO: implement nextResponder
struct CustomTextField: UIViewRepresentable {
    
    class Coordinator: NSObject, UITextFieldDelegate {
        
        @Binding var text: String
        @Binding var nextResponder: Bool?
        @Binding var isResponder: Bool?
        
        init(text: Binding<String>, nextResponder: Binding<Bool?>, isResponder: Binding<Bool?>) {
            _text = text
            _isResponder = isResponder
            _nextResponder = nextResponder
        }
        
        func textFieldDidChangeSelection(_ textField: UITextField) {
            text = textField.text ?? ""
        }
        
        func textFieldDidBeginEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.isResponder = true
            }
        }
        
        func textFieldDidEndEditing(_ textField: UITextField) {
            DispatchQueue.main.async {
                self.isResponder = false
                if self.nextResponder != nil {
                    self.nextResponder = true
                }
                else {
                    textField.resignFirstResponder()
                }
            }
        }
    }
    
    @Binding var text: String
    @Binding var nextResponder: Bool?
    @Binding var isResponder: Bool?
    
    init(text: Binding<String>, placeholder: String = "", isResponder: Binding<Bool?> = .constant(nil), nextResponder: Binding<Bool?> = .constant(nil), isSecured: Bool = false, keyboard: UIKeyboardType = .default) {
        _text = text
        _isResponder = isResponder
        _nextResponder = nextResponder
        self.placeholder = placeholder
        self.isSecured = isSecured
        self.keyboard = keyboard
    }
    
    let placeholder: String
    let isSecured: Bool
    let keyboard: UIKeyboardType
    
    func makeUIView(context: UIViewRepresentableContext<CustomTextField>) -> UITextField {
        let textField = UITextField(frame: .zero)
        textField.placeholder = placeholder
        textField.isSecureTextEntry = isSecured
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.keyboardType = keyboard
        textField.delegate = context.coordinator
        return textField
    }
    
    func makeCoordinator() -> CustomTextField.Coordinator {
        return Coordinator(text: $text, nextResponder: $nextResponder, isResponder: $isResponder)
    }
    
    func updateUIView(_ uiView: UITextField, context: UIViewRepresentableContext<CustomTextField>) {
        uiView.text = text
        if isResponder ?? false {
            uiView.becomeFirstResponder()
        }
    }
    
}
