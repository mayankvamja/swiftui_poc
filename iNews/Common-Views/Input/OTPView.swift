//
//  OTPView.swift
//  iNews
//
//  Created by Mayank Vamja on 10/06/21.
//

import SwiftUI

// TODO: NOT IMPLEMENTED
struct OTPView: View {
    
    @Binding var otp: String
    @State var text: [String] = []
    
    enum LetterType {
        case digits
        case alphabets
        case alphanumeric
    }
    
    private let noOfLetters: Int
    private let letterType: LetterType
    private let items: [GridItem]
    
    init(_ otp: Binding<String>,
         length noOfLettersInOTP: Int = 4,
         letterType: LetterType = .digits) {
        
        self.noOfLetters = noOfLettersInOTP
        self.items = Array(repeating: .init(.flexible(minimum: 80, maximum: 120)), count: noOfLettersInOTP)
        self.letterType = letterType
        self._otp = otp
        
        text = Array(repeating: "", count: noOfLetters)
    }
    
    var body: some View {
        LazyHGrid(rows: items, alignment: .center, spacing: 20, pinnedViews: [], content: {
            ForEach(0 ..< noOfLetters) { _ in
                TextInput($text[0], fieldType: .normal, validation: nil)
            }
        })
    }
}

struct OTPView_Previews: PreviewProvider {
    static var previews: some View {
        OTPView(.constant(""))
    }
}
