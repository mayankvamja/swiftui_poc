//
//  TextInput.swift
//  iNews
//
//  Created by Mayank Vamja on 10/06/21.
//

import SwiftUI

/// TextInput with `border` when editing
struct TextInput: View {
    @Binding var text: String
    @State var borderColor: Color = .gray
    
    private let placeholder: String
    private let style: Style
    private let type: FieldType
    private var validation: ValidationHandler?
    @State private var error: String?
    @State private var isEditing: Bool = false
    
    enum Style {
        case rounded(CGFloat)
        case capsule
    }
    
    enum FieldType {
        case secure
        case normal
    }

    typealias ValidationHandler = (_ input: String) -> (valid: Bool, error: String?)
    
    init(_ text: Binding<String>, fieldType: FieldType = .normal, label: String = "", style: Style = .rounded(10), validation: ValidationHandler?) {
        self.type = fieldType
        self.placeholder = label
        self._text = text
        self.style = style
        self.validation = validation
    }
    
    func onChange(_ value: String) {
        guard let result = self.validation?(text) else { return }
        self.error = (result.valid) ? nil : result.error
        borderColor = (result.valid)
            ? (isEditing ? .accentColor : .gray)
            : .red
    }
    
    var body: some View {
        
        if type == .normal {
            TextField(placeholder, text: $text) { self.isEditing = $0 }
                .onChange(of: text, perform: onChange(_:))
                .textFieldStyle(PlainTextFieldStyle())
                .padding()
                .background(style.background(borderColor))
                .modifier(FieldError($error))
            
        }
        else {
            SecureField(placeholder, text: $text)
                .onChange(of: text, perform: onChange(_:))
                .textFieldStyle(PlainTextFieldStyle())
                .padding()
                .background(style.background(borderColor))
                .modifier(FieldError($error))

        }
        
    }
}

struct FieldError: ViewModifier {
    @Binding var error: String?
    
    init(_ err: Binding<String?>) {
        self._error = err
    }
    
    func body(content: Content) -> some View {
        VStack(alignment: .leading) {
            content
            
            if error != nil {
                HStack {
                    Text(Image(systemName: "info.circle"))
                    Text(error!)
                }
                .foregroundColor(.red)
                .padding(.top, 5)
            }
            
        }

    }
}

extension TextInput.Style {
    func background(_ color: Color = .gray, _ width: CGFloat = 2) -> AnyView {
        switch self {
        case .rounded(let corner):
            return AnyView(RoundedRectangle(cornerRadius: CGFloat(corner), style: .circular).stroke(color, style: StrokeStyle(lineWidth: width)))
        case .capsule:
            return AnyView(Capsule().stroke(color, style: StrokeStyle(lineWidth: width)))
        }
    }
}

struct TextInput_Previews: PreviewProvider {
    @State var text: String = ""
    static var previews: some View {
        TextInput(.constant(""), label: "Enter text", validation: nil).padding()
    }
}
