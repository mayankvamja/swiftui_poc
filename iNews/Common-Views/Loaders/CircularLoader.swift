//
//  CircularLoader.swift
//  iNews
//
//  Created by Mayank Vamja on 14/05/21.
//

import SwiftUI

struct CircularLoader: View {
    
    @State private var shouldAnimate = false
    
    let color: Color
    
    var body: some View {
        Circle()
            .fill(Color.blue)
            .frame(width: 30, height: 30)
            .overlay(
                ZStack {
                    Circle()
                        .stroke(color, lineWidth: 100)
                        .scaleEffect(shouldAnimate ? 1 : 0)
                    Circle()
                        .stroke(color, lineWidth: 100)
                        .scaleEffect(shouldAnimate ? 1.5 : 0)
                    Circle()
                        .stroke(color, lineWidth: 100)
                        .scaleEffect(shouldAnimate ? 2 : 0)
                }
                .opacity(shouldAnimate ? 0.0 : 0.2)
                .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: false))
            )
            .onAppear {
                self.shouldAnimate = true
            }
    }
}

#if DEBUG
struct CircularLoader_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            CircularLoader(color: .blue)
        }
    }
}
#endif
