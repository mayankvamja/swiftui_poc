//
//  LoadingTextAnimated.swift
//  iNews
//
//  Created by Mayank Vamja on 14/05/21.
//

import SwiftUI

public struct ShimmerPlaceholder: View {
    
    public struct Constants {
        let duration: Double
        let minOpacity: Double
        let maxOpacity: Double
        let cornerRadius: CGFloat
    }
    
    private let constants: Constants
    private let color: Color
    @State private var opacity: Double = 0.25
    
    public init(_ color: Color = .gray, duration: Double = 0.75, minOpacity: Double = 0.25, maxOpacity: Double = 0.75, cornerRadius: CGFloat = 8.0) {
        self.color = color
        self.constants = Constants(duration: duration, minOpacity: minOpacity, maxOpacity: maxOpacity, cornerRadius: cornerRadius)
    }
    
    public var body: some View {
        RoundedRectangle(cornerRadius: constants.cornerRadius)
            .fill(color)
            .opacity(opacity)
            .transition(.opacity)
            .onAppear {
                let baseAnimation = Animation.easeInOut(duration: constants.duration)
                let repeated = baseAnimation.repeatForever(autoreverses: true)
                withAnimation(repeated) {
                    self.opacity = constants.maxOpacity
                }
        }
    }
}

struct LazyListItem: View {
    
    var body: some View {
        HStack(spacing: 20) {
            ShimmerPlaceholder(cornerRadius: 16)
                .frame(width: 80, height: 80)
            
            VStack(spacing: 10) {
                ShimmerPlaceholder(cornerRadius: 4)
                    .frame(height: 20)
                VStack(alignment: .leading, spacing: 5) {
                    ShimmerPlaceholder(cornerRadius: 4)
                        .frame(height: 14)
                    ShimmerPlaceholder(cornerRadius: 4)
                        .frame(maxWidth: 150, maxHeight: 14)
                }
            }
        }
        .frame(maxWidth: 500)
    }
}

#if DEBUG
struct ShimmerPlaceholder_Previews: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 20) {
            
            HStack(spacing: 20) {
                ShimmerPlaceholder(cornerRadius: 16)
                    .frame(width: 80, height: 80)
                
                VStack(spacing: 10) {
                    ShimmerPlaceholder(cornerRadius: 4)
                        .frame(height: 20)
                    VStack(alignment: .leading, spacing: 5) {
                        ShimmerPlaceholder(cornerRadius: 4)
                            .frame(height: 14)
                        ShimmerPlaceholder(cornerRadius: 4)
                            .frame(maxWidth: 150, maxHeight: 14)
                    }
                }
    
            }
            .frame(maxWidth: 500)
            
        }
        .preferredColorScheme(.dark)
        .padding(20)
    }
}
#endif
