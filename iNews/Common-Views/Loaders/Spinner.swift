//
//  Spinner.swift
//  iNews
//
//  Created by Mayank Vamja on 14/05/21.
//

import SwiftUI

struct Spinner: View {
    
    let colors: [Color]
    let animationTime: Double
    let rotationTime: Double
    let strokeWidth: CGFloat
    
    let fullRotation: Angle = .degrees(360)
    static let initialDegree: Angle = .degrees(270)
    
    @State var spinnerStart: CGFloat = 0.0
    @State var spinnerEndS1: CGFloat = 0.03
    @State var spinnerEndS2S3: CGFloat = 0.03
    
    @State var rotationDegreeS1 = initialDegree
    @State var rotationDegreeS2 = initialDegree
    @State var rotationDegreeS3 = initialDegree
    
    init(_ color1: Color, _ color2: Color, _ color3: Color, rotationTime: Double = 0.75, strokeWidth: CGFloat = 10) {
        self.colors = [color1, color2, color3]
        self.rotationTime = rotationTime
        self.animationTime = rotationTime * 2
        self.strokeWidth = strokeWidth
    }
    
    init(_ color1: Color, _ color2: Color, rotationTime: Double = 0.75, strokeWidth: CGFloat = 10) {
        self.init(color1, color2, color1, rotationTime: rotationTime)
    }
    
    init(_ color1: Color = .blue, rotationTime: Double = 0.75, strokeWidth: CGFloat = 10) {
        self.init(color1, color1.opacity(0.75), color1.opacity(0.25), rotationTime: rotationTime)
    }
    
    private struct SpinnerCircle: View {
        var start: CGFloat
        var end: CGFloat
        var rotation: Angle
        var color: Color
        let stroke: CGFloat
        
        var body: some View {
            Circle()
                .trim(from: start, to: end)
                .stroke(style: StrokeStyle(lineWidth: stroke, lineCap: .round))
                .fill(color)
                .rotationEffect(rotation)
        }
    }
    
    var body: some View {
        ZStack {
            
            SpinnerCircle(start: spinnerStart, end: spinnerEndS2S3, rotation: rotationDegreeS2, color: colors[1], stroke: strokeWidth)
            
            SpinnerCircle(start: spinnerStart, end: spinnerEndS2S3, rotation: rotationDegreeS3, color: colors[2], stroke: strokeWidth)
            
            SpinnerCircle(start: spinnerStart, end: spinnerEndS1, rotation: rotationDegreeS1, color: colors[0], stroke: strokeWidth)
            
        }
        .onAppear {
            self.animateSpinner()
            Timer.scheduledTimer(withTimeInterval: animationTime, repeats: true) { (_) in
                self.animateSpinner()
            }
        }
    }
    
    // MARK: Animation methods
    private func animateSpinner(with timeInterval: Double, completion: @escaping (() -> Void)) {
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { _ in
            withAnimation(Animation.easeInOut(duration: rotationTime)) {
                completion()
            }
        }
    }
    
    private func animateSpinner() {
        animateSpinner(with: rotationTime) { self.spinnerEndS1 = 1.0 }
        
        animateSpinner(with: (rotationTime * 2) - 0.025) {
            self.rotationDegreeS1 += fullRotation
            self.spinnerEndS2S3 = 0.8
        }
        
        animateSpinner(with: (rotationTime * 2)) {
            self.spinnerEndS1 = 0.2
            self.spinnerEndS2S3 = 0.2
        }
        
        animateSpinner(with: (rotationTime * 2) + 0.2) { self.rotationDegreeS2 += fullRotation }
        
        animateSpinner(with: (rotationTime * 2) + 0.3) { self.rotationDegreeS3 += fullRotation }
    }
    
}

#if DEBUG
struct Spinner_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spinner(.blue, .red, .yellow, rotationTime: 0.75)
                .frame(width: 100, height: 100, alignment: .center)
        }
    }
}
#endif

