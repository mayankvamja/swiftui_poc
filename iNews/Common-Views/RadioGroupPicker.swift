//
//  RadioGroupPicker.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import SwiftUI

/// Radio group picker with allowed only one selection
struct HRadioGroupPicker<
    Data: RandomAccessCollection,
    Content: View,
    SelectedContent: View
>: View where Data.Element: Hashable {
    
    @Binding private var selection: Data.Element?
    private let collection: Data
    private let alignment: HorizontalAlignment
    private let gridItems: [GridItem]
    private let spacing: CGFloat
    private let allowDeselction: Bool
    var itemView: (Data.Element) -> Content
    var selected: (Data.Element) -> SelectedContent
    
    ///  variant for ScrollView/List
    /// `= CGFloat.infinity` variant for VStack
    @State private var totalHeight = CGFloat.zero
    
    // Remove unused properties
    init(selection: Binding<Data.Element?>, data: Data, alignment: HorizontalAlignment = .center, size: GridItem.Size = .flexible(minimum: 120), spacing: CGFloat = 4, allowDeselection: Bool = true, itemView: @escaping (Data.Element) -> Content, selected: @escaping (Data.Element) -> SelectedContent) {
        self._selection = selection
        self.collection = data
        self.alignment = alignment
        self.itemView = itemView
        self.selected = selected
        self.spacing = spacing
        self.allowDeselction = allowDeselection
        self.gridItems = Array(repeating: GridItem(size), count: 3)
    }
    
    var body: some View {
        VStack {
            GeometryReader { geometry in
                self.generateContent(in: geometry)
            }
        }.frame(height: totalHeight)
    }
    
    private func generateContent(in geometry: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(collection, id: \.self) { item in
                self.item(for: item)
                    .padding(spacing)
                    .alignmentGuide(.leading, computeValue: { itemSize in
                        if abs(width - itemSize.width) > geometry.size.width {
                            width = 0
                            height -= itemSize.height
                        }
                        let result = width
                        if item == collection.last! {
                            width = 0
                        }
                        else {
                            width -= itemSize.width
                        }
                        return result
                    })
                    .alignmentGuide(.top, computeValue: {_ in
                        let result = height
                        if item == collection.last! {
                            height = 0
                        }
                        return result
                    })
            }
        }.background(viewHeightReader($totalHeight))
    }
    
    func item(for item: Data.Element) -> some View {
        Button {
            if selection == item {
                if allowDeselction {
                    selection = nil
                }
            }
            else {
                selection = item
            }
        }
        label: {
            if item == selection {
                selected(item)
                    .foregroundColor(.white)
            }
            else {
                itemView(item)
                    .foregroundColor(.black)
            }
        }
    }
    
    private func viewHeightReader(_ binding: Binding<CGFloat>) -> some View {
        return GeometryReader { geometry -> Color in
            let rect = geometry.frame(in: .local)
            DispatchQueue.main.async {
                binding.wrappedValue = rect.size.height
            }
            return .clear
        }
    }
}

struct HRadioGroupPicker_Preview: PreviewProvider {
    static var previews: some View {
        HRadioGroupPicker(
            selection: .constant(7),
            data: [1,2,4,5,6,7,8,9,10],
            alignment: .center, size: .flexible(minimum: 20, maximum: 20),
            spacing: 5,
            allowDeselection: true
        ) { item in
            Text("\(item)")
                .padding()
                .background(Color.red)
        } selected: { item in
            Text("\(item)")
                .padding()
                .background(Color.green)
        }

    }
}
