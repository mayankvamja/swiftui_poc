//
//  WebView.swift
//  iNews
//
//  Created by Mayank Vamja on 22/06/21.
//

import SwiftUI
import WebKit

final class WebView: UIViewRepresentable {
    
    @Binding var url: URL?
    let configuration: WKWebViewConfiguration
    
    init(_ url: Binding<URL?>, configuration: WKWebViewConfiguration = WKWebViewConfiguration()) {
        self._url = url
        self.configuration = configuration
    }
    
//    init(_ urlString: String?, configuration: WKWebViewConfiguration = WKWebViewConfiguration()) {
//        self.configuration = configuration
//
//        guard let _str = urlString else {
//            self.url = nil
//            return
//        }
//
//        self.url = URL(string: _str)
//    }
    
    func makeUIView(context: Context) -> WKWebView {
        let webview = WKWebView(frame: .zero, configuration: configuration)
        return webview
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let url = url else {
            return
        }
        
        uiView.load(URLRequest(url: url))
    }
}
