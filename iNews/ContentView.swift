//
//  ContentView.swift
//  iNews
//
//  Created by Mayank Vamja on 24/05/21.
//

import SwiftUI

struct TodoItem: Identifiable {
    let id: Int
    let text: String
}

struct ContentView: View {
    
    private let data: [TodoItem] = {
        var _data = [TodoItem]()
        for i in 0...30 {
            _data.append(TodoItem(id: i, text: "todo \(i * i)"))
        }
        return _data
    }()
    
    var body: some View {
        NavigationView {
            LazyVStack(alignment: .center) {
                
                ForEach(0..<10) { _ in
                    Group {
                        LazyListItem()
                        Divider()
                    }
                }
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
