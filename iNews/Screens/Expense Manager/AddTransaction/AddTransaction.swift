//
//  AddTransaction.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import SwiftUI

struct AddTransaction: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @StateObject private var viewModel: TransactionViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var mode: String {
        viewModel.transactionType.rawValue
    }
    
    init(transaction: Transactions? = nil) {
        self._viewModel = StateObject(wrappedValue: TransactionViewModel(transaction: transaction))
    }
    
    var body: some View {
        Form {
            
            Section(header: Text("Tranaction type")) {
                Picker("Tranaction type", selection: $viewModel.transactionType) {
                    ForEach(TransactionType.allCases, id: \.self) { type in
                        Text(type.rawValue)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            Section(header: Text("\(mode) title")) {
                TextField("Title (e.g: \(viewModel.transactionType == .expense ? "Movie Tickets" : "Salary")", text: $viewModel.title)
            }
            
            Section(header: Text("\(mode) DETAILS")) {
                TextField("Amount", text: $viewModel.amount)
                    .onChange(of: viewModel.amount, perform: viewModel.handleAmountChange(_:))
                
                if viewModel.transactionType == .expense {
                    
                    Picker("Category", selection: $viewModel.expenseCategory) {
                        ForEach(ExpenseTag.allCases, id: \.self) { type in
                            HStack(spacing: 20) {
                                Image(systemName: type.imageName)
                                    .padding(10)
                                    .foregroundColor(Color(type.color))
                                    .background(Color(type.color).opacity(0.2))
                                    .clipShape(Circle())
                                
                                Text(type.rawValue)
                            }.padding(10)
                        }
                    } // Expense Category Picker
                    
                    Picker("Transaction mode", selection: $viewModel.transactionMode) {
                        ForEach(TransactionMode.allCases, id: \.self) { type in
                            Text(type.rawValue)
                        }
                    } // Transaction mode Picker
                    
                }
                else {
                    Picker("Category", selection: $viewModel.incomeCategory) {
                        ForEach(IncomeTag.allCases, id: \.self) { type in
                            Text(type.rawValue)
                        }
                    } // Transaction mode Picker
                }
                
                DatePicker(selection: $viewModel.transactionDate, label: {
                    Text("Date")
                })
                
            }
            
            Section(header: Text("Note")) {
                
                // TODO: Fix auto height issue
                TextEditor(text: $viewModel.note)
            }
            
            Section(header: Text("Image")) {
                if let uiimage = viewModel.image {
                    Button(action: {
                        withAnimation(.spring()) {
                            viewModel.removeImage()
                        }
                    }) {
                        Label("Remove Image", systemImage: "link")
                    }

                    Image(uiImage: uiimage)
                        .resizable()
                        .scaledToFit()
                        .frame(maxHeight: 300)
                }
                else {
                    Button(action: {viewModel.showImagePicker = true}, label: {
                        Label("Select Image", systemImage: "link")
                    })
                }
            }
            
        }
        .alert(isPresented: $viewModel.showError, content: {
            Alert(title: Text(viewModel.errorMsg))
        })
        .sheet(isPresented: $viewModel.showImagePicker, content: {
            ImagePicker(image: $viewModel.image)
        })
        .navigationBarTitle(Text(viewModel.navigationTitle))
        .navigationBarItems(
            trailing: Button(action: {
                viewModel.save(in: viewContext) { success in
                    if success {
                        presentationMode.wrappedValue.dismiss()
                    }
                }
            }) {
                Text("Save").fontWeight(.bold)
            }
        )
    }
}

struct AddTransaction_Previews: PreviewProvider {
    static var previews: some View {
        AddTransaction()
    }
}
