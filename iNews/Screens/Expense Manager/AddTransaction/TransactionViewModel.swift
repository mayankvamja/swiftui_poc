//
//  TransactionViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import CoreData
import Combine
import UIKit.UIImage

class TransactionViewModel: ObservableObject {
    
    private var object: Transactions?
    
    @Published var navigationTitle: String
    @Published var title: String = ""
    @Published var amount: String = ""
    @Published var note: String = ""
    
    @Published var errorMsg: String = ""
    @Published var showError: Bool = false
    @Published var showImagePicker: Bool = false
    @Published var image: UIImage? = nil

    @Published var transactionType: TransactionType = .expense
    @Published var incomeCategory: IncomeTag = .none
    @Published var expenseCategory: ExpenseTag = .Other
    @Published var transactionMode: TransactionMode = .none
    @Published var transactionDate: Date = Date()
    
    // ATTACTHMENT
    @Published var hasAttatchment: Bool = false
    @Published var attatchmentPath: String? = nil
    
    init(transaction: Transactions? = nil) {
        self.object = transaction
        if transaction == nil {
            self.navigationTitle = "Add".localized + " Transaction".localized
        }
        else {
            self.navigationTitle = "Edit".localized + " Transaction".localized
            transactionType = TransactionType(rawValue: transaction?.transactionType ?? "Expense")!
            incomeCategory = IncomeTag(rawValue: transaction?.tag ?? "") ?? .none
            expenseCategory = ExpenseTag(rawValue: transaction?.tag ?? "") ?? .Other
            transactionMode = TransactionMode(rawValue: transaction?.transactionMode ?? "") ?? .none
            
            transactionDate = transaction?.transactionDate ?? Date()
            title = transaction?.title ?? ""
            amount = String(format: "%.2f", transaction?.amount ?? 0)
            note = transaction?.note ?? ""
            image = UIImage(data: transaction?.attatchment ?? Data())
        }
    }
    
    func removeImage() {
        image = nil
    }
    
    func handleAmountChange(_ value: String) {
        if value.count > 0 && Double(value) == nil {
            amount.removeLast()
            handleAmountChange(amount)
        }
    }
    
    func save(in context: NSManagedObjectContext, completion: @escaping (Bool) -> Void) {
        guard validate() else {
            completion(false)
            return
        }
        
        if object == nil {
            object = Transactions(context: context)
            object?.createdAt = Date()
        }
        
        object?.amount = Double(amount)!
        object?.title = title
        object?.note = note
        object?.transactionDate = transactionDate
        object?.transactionType = transactionType.rawValue
        object?.transactionMode = (transactionType == .income) ? nil : transactionMode.rawValue
        object?.tag = (transactionType == .expense) ? expenseCategory.rawValue : incomeCategory.rawValue
        object?.attatchment = image?.pngData()
        object?.updatedAt = Date()
        
        context.save(completion: completion)
    }
    
    func validate() -> Bool {
        guard title.trimmingCharacters(in: .whitespaces).count > 0 else {
            errorMsg = "Title should not be empty".localized
            showError = true
            return false
        }
        
        guard (Double(amount) ?? 0) > 0 else {
            errorMsg = "Please write amount of transaction".localized
            showError = true
            return false
        }
        
        return true
    }
}

extension NSManagedObjectContext {
    func save(completion: ((Bool) -> Void)? = nil) {
        do {
            try self.save()
            completion?(true)
        }
        catch {
            completion?(false)
        }
    }
}
