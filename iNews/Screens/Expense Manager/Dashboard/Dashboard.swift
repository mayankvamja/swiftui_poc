//
//  Dashboard.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import SwiftUI
import CoreData

struct Dashboard: View {
    
    @StateObject var viewModel = DashboardViewModel()
    @Binding var tab: Int
    @Environment(\.managedObjectContext) var viewContext
    @Environment(\.horizontalSizeClass) var horizontalSizeClass

    private var stackViewContent: some View {
        Group {
            DashboardChartView(viewModel)
            
            Divider()
            
            GroupBox {
                if viewModel.recentTransactions.count > 0 {
                    
                    HStack {
                        Text("Recent Transactions")
                        Spacer()
                        Button(action: { tab = 2 }) {
                            HStack {
                                Text("View All")
                                Image(systemName: "chevron.right")
                            }
                        }
                    }
                    Rectangle()
                        .fill(Color.gray)
                        .frame(height: 2)
                }
                
                ForEach(viewModel.recentTransactions) { item in
                    
                    NavigationLink("", destination: ExpenseDetails(transaction: item), tag: item.id, selection: $viewModel.selectedListItem).hidden()
                    
                    CompactTransactionListItem(value: item)
                        .onTapGesture {
                            viewModel.selectedListItem = item.id
                        }
                    
                    if item != viewModel.recentTransactions.last {
                        Divider()
                    }
                }
                
            }
            .frame(maxWidth: 400)
            .groupBoxStyle(AppGroupBox(.white))
        }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.backgroundColor.ignoresSafeArea()
                
                ScrollView(.vertical) {
                    if horizontalSizeClass == .compact {
                        VStack(alignment: .leading, spacing: 15) {
                            
                            Divider()
                            stackViewContent
                            
                        }
                        .padding()
                        .padding(.bottom, 100)
                    }
                    else {
                        HStack(alignment: .top, spacing: 25) {
                            stackViewContent
                        }
                        .padding()
                    }
                }
                
                FloatingAddButton()
                
            } // ZStack
            .onAppear(perform: {
                print("onAppear loadRecentTransactions")
                viewModel.loadRecentTransactions(context: viewContext)
            })
            .navigationTitle("Dashboard")
            .navigationBarItems(trailing: HStack(spacing: 15) {
                
            })
            .navigationBarBackButtonHidden(true)
        }
        .navigationViewStyle(StackNavigationViewStyle())

    }
}

struct DashboardCard: View {
    let image: Image
    let radius: CGFloat
    let color: Color
    let title: String
    let subtitle: String
    
    var body: some View {
        VStack {
            
            image
                .resizable()
                .frame(width: 20, height: 20)
                .padding()
                .foregroundColor(color)
                .background(Circle().fill(color.opacity(0.2)))
            
            Text(title)
                .font(.title3)
                .fontWeight(.bold)
            
            Text(subtitle)
                .font(.title3)
                .fontWeight(.semibold)
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.white)
        .cornerRadius(radius)
        .overlay(RoundedRectangle(cornerRadius: radius)
                    .stroke(color, lineWidth: 2))
    }
}

struct DashboardChartView: View {
    
    @ObservedObject var viewModel: DashboardViewModel
    @Environment(\.managedObjectContext) var context
    
    private var fetchRequest: FetchRequest<Transactions>
    private var data: FetchedResults<Transactions> {
        fetchRequest.wrappedValue
    }
    
    init(_ viewModel: DashboardViewModel) {
        self.viewModel = viewModel
        self.fetchRequest = FetchRequest(
            fetchRequest: Transactions.fetchTransactions(filterTime: viewModel.timeFilterSelected ?? .month)
        )
        
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            HRadioGroupPicker(selection: $viewModel.timeFilterSelected, data: ExpenseFilterTime.allCases, allowDeselection: false) { (item) in
                Label(item.rawValue, systemImage: "circle")
                    .padding(5)
                    .background(Color.white)
                    .cornerRadius(5)
            } selected: { item in
                Label(item.rawValue, systemImage: "checkmark.circle.fill")
                    .padding(5)
                    .background(LinearGradient(gradient: Gradient(colors: [.primaryColor, .secondaryColor]), startPoint: .leading, endPoint: .trailing))
                    .cornerRadius(5)
            }
            
            /// Dashboard
            HStack(spacing: 12) {
                DashboardCard(image: .init(systemName: "arrow.up"), radius: 20, color: .red, title: "Expenses", subtitle: "\(String(format: "%.2f", data.expenses.sum))")
                
                DashboardCard(image: .init(systemName: "arrow.down"), radius: 20, color: .green, title: "Income", subtitle: "\(data.incomes.sum)")
            }
            
//            LineChart(title: "Expenses Chart")
//                .hiddenGrid(true)
//                .chartStyle(.curved)
//                .background(Color.white)
//                .cornerRadius(20)
//                .frame(height: 300)
                        
            if viewModel.chartData.count > 0 {
                BarChart("Expenses", $viewModel.chartData)
                    .frame(height: 400)
                    .padding()
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: .shadowColor, radius: 10, x: 0, y: 0)
            }
            
        }
        .frame(maxWidth: 400)
        .onAppear {
            viewModel.loadChardData(by: viewModel.timeFilterSelected, context: context)
        }
        .onChange(of: viewModel.timeFilterSelected, perform: { value in
            viewModel.loadChardData(by: value, context: context)
        })
    }
}

struct Dashboard_Previews: PreviewProvider {
    private static let context = PersistanceController.transactions.container.viewContext
    static var previews: some View {
        Dashboard(tab: .constant(1)).environment(\.managedObjectContext, context)
    }
}
