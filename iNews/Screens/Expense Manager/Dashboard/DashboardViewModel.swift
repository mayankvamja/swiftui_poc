//
//  DashboardViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 22/06/21.
//

import SwiftUI
import Combine
import CoreData.NSManagedObjectContext

class DashboardViewModel: ObservableObject {
    
    @Published var recentTransactions: [Transactions] = []
    @Published var chartData: [BarChart.Item] = []
    @Published var timeFilterSelected: ExpenseFilterTime? = .month
    @Published var selectedListItem: ObjectIdentifier? = nil
        
    func loadRecentTransactions(context: NSManagedObjectContext) {
        Transactions.fetchTransactions(fetchLimit: 5, context: context) { [weak self] (data) in
            self?.recentTransactions = data
        }
    }
    
    func loadChardData(type: TransactionType = .expense, by time: ExpenseFilterTime?, context: NSManagedObjectContext) {
        guard let _filter = time else {
            chartData = []
            return
        }
        Transactions.fetchChartData(type, context: context, by: _filter) { [weak self] (data) in
            self?.chartData = data.map({ (item) -> BarChart.Item in
                let color = Color(ExpenseTag(rawValue: item.tag)?.color ?? IncomeTag(rawValue: item.tag)?.color ?? 0x545454)
                let icon = ExpenseTag(rawValue: item.tag)?.imageName ?? IncomeTag(rawValue: item.tag)?.imageName ?? "info.circle"
                let label = Label(item.tag, systemImage: icon)
                return .init(item.sum, label: label, color: color)
            })
        }
    }
}
