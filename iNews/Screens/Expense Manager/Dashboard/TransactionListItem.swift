//
//  TransactionListItem.swift
//  iNews
//
//  Created by Mayank Vamja on 24/06/21.
//

import SwiftUI

struct CompactTransactionListItem: View {
    let transaction: Transactions
    let type: TransactionType
    let icon: (name: String, color: Color)
    
    init(value: Transactions) {
        self.transaction = value
        self.type = TransactionType(rawValue: transaction.transactionType ?? "") ?? .expense
        
        if type == .expense {
            let tag = ExpenseTag(rawValue: transaction.tag ?? "") ?? .Other
            self.icon = (name: tag.imageName, color: Color(tag.color))
        }
        else {
            let tag = IncomeTag(rawValue: transaction.tag ?? "") ?? .none
            self.icon = (name: tag.imageName, color: Color(tag.color))
        }
    }
    
    var amountUtils: (String, Color) {
        if type == .expense {
            return ("arrow.up", .red)
        }
        else {
            return ("arrow.down", .green)
        }
    }
    
    var body: some View {
        HStack {
            Text(transaction.title ?? "-")
                .font(.system(size: 22, weight: .regular, design: .rounded))
            
            Spacer()
            Label(String(format: "%.2f", transaction.amount), systemImage: amountUtils.0)
                .foregroundColor(amountUtils.1)
                .font(.system(size: 16, weight: .bold, design: .monospaced))
        }
        .padding(.horizontal)
    }
}

struct TransactionListItem: View {
    let transaction: Transactions
    let type: TransactionType
    let icon: (name: String, color: Color)
    
    init(value: Transactions) {
        self.transaction = value
        self.type = TransactionType(rawValue: transaction.transactionType ?? "") ?? .expense
        
        if type == .expense {
            let tag = ExpenseTag(rawValue: transaction.tag ?? "") ?? .Other
            self.icon = (name: tag.imageName, color: Color(tag.color))
        }
        else {
            let tag = IncomeTag(rawValue: transaction.tag ?? "") ?? .none
            self.icon = (name: tag.imageName, color: Color(tag.color))
        }
    }
    
    var amountUtils: (String, Color) {
        if type == .expense {
            return ("arrow.up", .red)
        }
        else {
            return ("arrow.down", .green)
        }
    }
    
    var body: some View {
        HStack(spacing: 20) {
            Image(systemName: icon.name)
                .font(.system(size: 25))
                .padding()
                .foregroundColor(icon.color)
                .background(icon.color.opacity(0.2))
                .clipShape(Circle())
            
            VStack(alignment: .leading) {
                HStack {
                    Text(transaction.title ?? "-")
                        .font(.title2)
                    
                    Spacer()
                    
                    Group {
                        Text(Image(systemName: amountUtils.0))
                        Text(String(format: "%.2f", transaction.amount))
                            .fontWeight(.bold)
                    }
                    .foregroundColor(amountUtils.1)
                }
                Text(transaction.tag ?? "-")
                    .foregroundColor(.gray)
                    .font(.body)
                
                Text(transaction.transactionDate?.humanReadable ?? "")
                    .font(.callout)
            }
        }
        .padding()
        .background(Color.white)
        .cornerRadius(15)
        .shadow(color: .shadowColor, radius: 2, x: 0, y: 0)
    }
}
