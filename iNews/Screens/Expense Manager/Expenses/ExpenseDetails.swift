//
//  ExpenseDetails.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import SwiftUI

struct ExpenseDetails: View {
        
    @ObservedObject var transaction: Transactions
    
    func detail(_ title: String, _ subtitle: String) -> some View {
        // : check for frame -> maxWidth use
        VStack(alignment: .leading) {
            Text(title).font(.body).foregroundColor(.gray)
            Text(subtitle).font(.title3)
                .multilineTextAlignment(.leading)
                
        }.padding()
        .frame(maxWidth: .infinity, alignment: .leading)
        
    }
    
    var body: some View {
        ZStack {
            
            ScrollView {
                VStack(spacing: 0) {
                    
                    detail("Title", transaction.title ?? "-")
                    
                    Rectangle().fill(Color.white).frame(height: 6)

                    HStack {
                        detail("Amount", String(format: "%.2f", transaction.amount))
                        
                        Rectangle().fill(Color.white).frame(width: 6)
                        
                        detail("Category", transaction.tag ?? "-")
                    }
                    .fixedSize(horizontal: false, vertical: true)
                    
                    Rectangle().fill(Color.white).frame(height: 6)

                    HStack {
                        detail("Date", transaction.transactionDate?.formated() ?? "-")
                        
                        Rectangle().fill(Color.white).frame(width: 6)
                        
                        detail("Last modified", transaction.updatedAt?.humanReadable ?? "-")
                    }
                    .fixedSize(horizontal: false, vertical: true)
                    
                    Rectangle().fill(Color.white).frame(height: 6)

                    HStack {
                        detail("Transaction Mode", transaction.transactionMode ?? "-")
                    }
                    .fixedSize(horizontal: false, vertical: true)
                    
                    Rectangle().fill(Color.white).frame(height: 6)

                    detail("Note", transaction.note ?? "- -")
                    
                }
                .background(Color.backgroundColor)
                .cornerRadius(20)
                .padding()
                .navigationBarTitle(transaction.transactionType ?? "Transaction")
            }
            
            VStack {
                Spacer()
                HStack {
                    Spacer()
                    
                    NavigationLink(
                        destination: AddTransaction(transaction: transaction),
                        label: {
                            Label("Edit", systemImage: "pencil")
                                .font(.system(size: 22, weight: .bold, design: .rounded))
                                .padding()
                                .foregroundColor(.white)
                                .background(Capsule().fill(Color.primaryColor))
                        })
                    
                }.padding()
            }
        }
    }
}
