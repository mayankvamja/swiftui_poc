//
//  ExpenseFilters.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import Foundation
import SwiftUI

struct ExpenseFiltersView: View {
    
    @Environment(\.presentationMode) private var presentationMode
    @ObservedObject private var viewModel: ExpensesViewModel
    
    private enum TRType: String, Hashable, CaseIterable {
        case expense = "Expense"
        case income = "Income"
        case all = "All"
        
        init(transactionType: TransactionType?) {
            switch transactionType {
            case .expense:
                self = .expense
            case .income:
                self = .income
            case nil:
                self = .all
            }
        }
        
        var transactionType: TransactionType? {
            switch self {
            case .expense:
                return .expense
            case .income:
                return .income
            case .all:
                return nil
            }
        }
    }
    
    @State private var type: TRType
    @State private var timeFilter: ExpenseFilterTime
    @State private var sortBy: ExpenseSort
    @State private var expenseTag: ExpenseTag?
    @State private var incomeTag: IncomeTag?
    @State private var tagsFilter: String?
    @State private var sortAscending: Bool
    
    init(viewModel: ExpensesViewModel) {
        self.viewModel = viewModel
        self._type = State(initialValue: TRType(transactionType: viewModel.typeFilter))
        self._timeFilter = State(initialValue: viewModel.timeFilter)
        self._sortBy = State(initialValue: viewModel.sortBy)
        self._expenseTag = State(initialValue: ExpenseTag(rawValue: viewModel.tagsFilter ?? ""))
        self._incomeTag = State(initialValue: IncomeTag(rawValue: viewModel.tagsFilter ?? ""))
        self._tagsFilter = State(initialValue: viewModel.tagsFilter)
        self._sortAscending = State(initialValue: viewModel.sortAscending)
    }
    
    func setup() {
        self.type = TRType(transactionType: viewModel.typeFilter)
        self.timeFilter = viewModel.timeFilter
        self.sortBy = viewModel.sortBy
        self.expenseTag = ExpenseTag(rawValue: viewModel.tagsFilter ?? "")
        self.incomeTag = IncomeTag(rawValue: viewModel.tagsFilter ?? "")
        self.tagsFilter = viewModel.tagsFilter
        self.sortAscending = viewModel.sortAscending
    }
    
    func apply() {
        
        switch type {
        case .expense:
            tagsFilter = expenseTag?.rawValue
        case .income:
            tagsFilter = incomeTag?.rawValue
        case .all:
            tagsFilter = nil
        }
        
        viewModel.applyFilters(with: timeFilter, typeFilter: type.transactionType, tagsFilter: tagsFilter, sortBy: sortBy, ascending: sortAscending)
        dismiss()
    }
    
    func reset() {
        viewModel.resetFilters()
        self.setup()
    }
    
    func dismiss() {
        presentationMode.wrappedValue.dismiss()
    }
    
    var body: some View {
        GeometryReader { gr in
            ScrollView {
                VStack {
                    
                    // Fills whatever space is left
                    Spacer()
                    
                    // : Why onTapGesture not working?
                    /*Rectangle()
                        .foregroundColor(.clear)
                        .onTapGesture {
                            dismiss()
                        }*/
                    
                    VStack {
                        Text("")
                            .frame(width: 50, height: 10)
                            .background(Capsule().fill(Color.gray.opacity(0.5)))
                        
                        HStack {
                            Label("Filters", systemImage: "slider.horizontal.3")
                                .foregroundColor(.primaryColor)
                                .font(.system(size: 25, weight: .bold))
                            
                            Spacer()
                            
                            Button(action: dismiss) {
                                Image(systemName: "xmark")
                                    .foregroundColor(.black)
                                    .padding()
                            }
                        }
                        
                        Divider()
                        
                        GroupBox(label: Text("Transaction type")) {
                            Picker(selection: $type, label: Text("Transaction type"), content: {
                                ForEach(TRType.allCases, id: \.self) { item in
                                    Text(item.rawValue)
                                }
                            }).pickerStyle(SegmentedPickerStyle())
                        }.groupBoxStyle(AppGroupBox())
                        
                        GroupBox(label: Text("Filter by time")) {
                            Picker(selection: $timeFilter, label: Text("Filter by time"), content: {
                                ForEach(ExpenseFilterTime.allCases, id: \.self) { item in
                                    Text(item.rawValue)
                                }
                            }).pickerStyle(SegmentedPickerStyle())
                        }.cornerRadius(20)
                        
                        GroupBox(label: HStack {
                            Text("Sort by")
                            Spacer()
                            Button(action: {sortAscending.toggle()}, label: {
                                Label(
                                    sortAscending ? "ascending" : "descending",
                                    systemImage: sortAscending ? "arrow.up" : "arrow.down"
                                )
                                .padding(.horizontal, 10)
                                .padding(.vertical, 5)
                                .background(Capsule().fill(Color.backgroundColor))
                                .background(Capsule().stroke(Color.primaryColor))
                                
                            })
                        }) {
                            Picker(selection: $sortBy, label: Text("Sort by time"), content: {
                                ForEach(ExpenseSort.allCases, id: \.self) { item in
                                    Text(item.rawValue)
                                }
                            }).pickerStyle(SegmentedPickerStyle())
                        }.cornerRadius(20)
                        
                        if type == .expense {
                            
                            GroupBox(label: Text("Filter by tags")) {
                                HRadioGroupPicker(selection: $expenseTag, data: ExpenseTag.allCases, alignment: .center) { item in
                                    Label(item.rawValue, systemImage: "circle")
                                        .padding(15)
                                        .background(Color.white)
                                        .cornerRadius(10)
                                } selected: { item in
                                    Label(item.rawValue, systemImage: "checkmark.circle.fill")
                                        .padding(15)
                                        .background(LinearGradient(gradient: Gradient(colors: [.primaryColor, .secondaryColor]), startPoint: .leading, endPoint: .trailing))
                                        .cornerRadius(10)
                                }
                            }.cornerRadius(20)
                            
                        }
                        else if type == .income {
                            
                            GroupBox(label: Text("Filter by tags")) {
                                HRadioGroupPicker(selection: $incomeTag, data: IncomeTag.allCases, alignment: .center) { item in
                                    Label(item.rawValue, systemImage: "circle")
                                        .padding(15)
                                        .background(Color.white)
                                        .cornerRadius(10)
                                } selected: { item in
                                    Label(item.rawValue, systemImage: "checkmark.circle.fill")
                                        .padding(15)
                                        .background(LinearGradient(gradient: Gradient(colors: [.primaryColor, .secondaryColor]), startPoint: .leading, endPoint: .trailing))
                                        .cornerRadius(10)
                                }
                            }.cornerRadius(20)
                            
                        }
                        
                        HStack(spacing: 10) {
                            Button(action: apply, label: {
                                Text("Apply Filters")
                                    .frame(maxWidth: .infinity)
                                    .font(.system(size: 25))
                                    .foregroundColor(.white)
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 20).fill(Color.primaryColor))
                            })
                            
                            Button(action: reset, label: {
                                Text("Clear")
                                    .font(.system(size: 25))
                                    .foregroundColor(.red)
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 20).fill(Color.red.opacity(0.1)))
                            })
                        }
                    }.padding()
                    .background(Color.white)
                    .cornerRadius(20)
                }
                
                // Makes the content stretch to fill the whole scroll view, but won't be limited (it can grow beyond if needed)
                .frame(minHeight: gr.size.height)
            }
        }
    }
}

struct AppGroupBox: GroupBoxStyle {
    
    let color: Color
    init(_ color: Color = .backgroundColor) {
        self.color = color
    }
    func makeBody(configuration: Configuration) -> some View {
        VStack(alignment: .leading) {
            configuration.label
                .foregroundColor(.darkColor)
                .font(.system(size: 18, weight: .semibold, design: .rounded))
            configuration.content
        }
        .frame(maxWidth: .infinity)
        .padding()
        .background(RoundedRectangle(cornerRadius: 20).fill(color))
    }
}

struct ExpensesFilters_Previews: PreviewProvider {
    static var previews: some View {
        Expenses()
    }
}
