//
//  Expenses.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import SwiftUI

struct Expenses: View {
    
    @Environment(\.managedObjectContext) var viewContext
    @StateObject private var viewModel = ExpensesViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                List {
                    
                    HStack {
                        
                        Image(systemName: "magnifyingglass")
                        TextField("Search", text: $viewModel.searchText)
                            .padding(15)
                            .modifier(ClearButton(text: $viewModel.searchText))
                        
                    }
                    .padding(.leading, 20)
                    .background(RoundedRectangle(cornerRadius: 20).stroke(Color.gray))
                    
                    FilteredExpenses(viewModel: viewModel)
                    
                }
                .navigationBarTitle("Expenses")
                .sheet(isPresented: $viewModel.showFiltersView, content: {
                    ExpenseFiltersView(viewModel: viewModel)
                        .background(BackgroundClearView())
                })
                
                FloatingAddButton()
                
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        
    }
}

struct BackgroundClearView: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        let view = UIView()
        DispatchQueue.main.async {
            view.superview?.superview?.backgroundColor = .clear
        }
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {}
}

struct Expenses_Previews: PreviewProvider {
    private static let context = PersistanceController.transactions.container.viewContext
    static var previews: some View {
        Expenses().environment(\.managedObjectContext, context)
    }
}
