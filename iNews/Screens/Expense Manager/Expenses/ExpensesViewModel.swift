//
//  ExpensesViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import SwiftUI
import CoreData.NSManagedObjectContext

class ExpensesViewModel: ObservableObject {
    
    @Published var timeFilter: ExpenseFilterTime = .all
    @Published var typeFilter: TransactionType? = nil
    @Published var tagsFilter: String? = nil
    @Published var sortBy: ExpenseSort = .transactionDate
    @Published var sortAscending: Bool = true
    
    @Published var showFiltersView: Bool = false
    @Published var searchText: String = ""
    
    func resetFilters() {
        timeFilter = .all
        typeFilter = nil
        tagsFilter = nil
        sortBy = .transactionDate
        sortAscending = true
    }
    
    func applyFilters(with timeFilter: ExpenseFilterTime, typeFilter: TransactionType?, tagsFilter: String?, sortBy: ExpenseSort, ascending: Bool) {
        self.timeFilter = timeFilter
        self.typeFilter = typeFilter
        self.tagsFilter = tagsFilter
        self.sortBy = sortBy
        self.sortAscending = ascending
    }
    
    func fetchTransactionsRequest() -> FetchRequest<Transactions> {
        return FetchRequest(fetchRequest: Transactions
                                .fetchTransactions(typeFilter,
                                                   sortBy: sortBy,
                                                   ascending: sortAscending,
                                                   filterTime: timeFilter,
                                                   filterText: searchText,
                                                   filterTag: tagsFilter
                                ))
    }
    
    func delete(transaction: Transactions, context: NSManagedObjectContext) {
        context.delete(transaction)
        context.save { (_) in
            print("Deleted Transaction")
        }
    }
}
