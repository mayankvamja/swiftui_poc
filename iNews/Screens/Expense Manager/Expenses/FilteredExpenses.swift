//
//  FilteredExpenses.swift
//  iNews
//
//  Created by Mayank Vamja on 15/06/21.
//

import SwiftUI

struct FilteredExpenses: View {
    
    @ObservedObject var viewModel: ExpensesViewModel
    @Environment(\.managedObjectContext) var context
    @State private var viewSelection: ObjectIdentifier? = nil
    @State private var editSelection: ObjectIdentifier? = nil
    private var fetchRequest: FetchRequest<Transactions>
    private var data: FetchedResults<Transactions> {
        fetchRequest.wrappedValue
    }
    
    init(viewModel: ExpensesViewModel) {
        self.viewModel = viewModel
        print("Filtering Transactions")
        fetchRequest = viewModel.fetchTransactionsRequest()
    }
    
    var body: some View {
        Group {
            HStack {
                Text("\(data.count) results")
                Spacer()
                Button(action: {
                    viewModel.showFiltersView = true
                }) {
                    Label("Filter results", systemImage: "slider.horizontal.3")
                        .foregroundColor(.secondaryColor)
                        .padding(.horizontal, 20)
                        .padding(.vertical, 10)
                        .background(Color.backgroundColor)
                        .cornerRadius(30)
                }
            }

            if data.count == 0 {
                Image("empty")
                    .resizable()
                    .padding()
                    .scaledToFit()
                    .frame(maxWidth: .infinity, maxHeight: 300)
            }
            
            ForEach(data) { item in
                VStack(spacing: 0) {
                    NavigationLink("", destination: ExpenseDetails(transaction: item), tag: item.id, selection: $viewSelection).frame(height: 0) .hidden()
                    
                    NavigationLink("Edit", destination: AddTransaction(transaction: item), tag: item.id, selection: $editSelection).frame(height: 0).hidden()
                    
                    TransactionListItem(value: item)
                        .onTapGesture {
                            viewSelection = item.id
                        }
                        .contextMenu(menuItems: {
                            Button("Edit") {
                                self.editSelection = item.id
                            }
                            Text("Share")
                        })
                    
                    //                    .background(Color.white)
                    //                    .cornerRadius(20)
                    //                    .shadow(color: .shadowColor, radius: 5, x: 0, y: 0)
                    
                }
                .padding(8)
                
            }
            .onDelete(perform: { indexSet in
                
                indexSet.forEach { (index) in
                    dump(data[index])
                    viewModel.delete(transaction: data[index], context: context)
                }
            })
            .listRowInsets(EdgeInsets())
            .background(Color.white)
        }
    }
}

struct FilteredExpenses_Previews: PreviewProvider {
    static var previews: some View {
        FilteredExpenses(viewModel: .init())
    }
}
