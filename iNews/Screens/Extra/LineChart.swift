//
//  LineChart.swift
//  iNews
//
//  Created by Mayank Vamja on 16/06/21.
//

import SwiftUI

// swiftlint:disable identifier_name
struct Point: Equatable {
    let x: CGFloat
    let y: CGFloat
    
    init(_ x: CGFloat, _ y: CGFloat) {
        self.x = x
        self.y = y
    }
    
    var cgpoint: CGPoint {
        .init(x: x, y: y)
    }
}

extension Array where Element == Point {
    var minXValue: CGFloat {
        self.map({$0.x}).min() ?? 0
    }
    
    var minYValue: CGFloat {
        self.map({$0.y}).min() ?? 0
    }
    
    var maxXValue: CGFloat {
        self.map({$0.x}).max() ?? 0
    }
    
    var maxYValue: CGFloat {
        self.map({$0.y}).max() ?? 0
    }
}

enum ChartError: LocalizedError {
    case minTwoPointsDataRequired
    case mismatchXYaxisDataSize
}

struct LineChart: View {
    
    enum Style {
        case straight
        case curved
    }
    
    let xStepValue: CGFloat
    let yStepValue: CGFloat
    let title: String
    
    @State private var girdsVisible: Bool = true
    @State private var style: Style = .straight
    @State private var radius: Float = 0.5
    
    var points: [Point]
    
    private var xStepsCount: Int {
        Int(self.points.maxXValue / self.xStepValue)
    }
    private var yStepsCount: Int {
        Int(self.points.maxYValue / self.yStepValue)
    }
    
    // : REMOVE DEFAULT VALUES
    init(title: String, points: [Point] = [Point(1, 5), Point(2, 4), Point(3, 15)], xStepValue: CGFloat = 0.5, yStepValue: CGFloat = 2) {
        self.title = title
        self.points = points
        self.xStepValue = xStepValue
        self.yStepValue = yStepValue
    }
    
//    init(xAxisData: [Double], xStepValue: CGFloat, yStepValue: CGFloat) throws {
//        if xAxisData.count < 2 {
//            throw ChartError.minTwoPointsDataRequired
//        }
//
//        let step = xAxisData.max()! / Double(xAxisData.count)
//        try self.init(points: xAxisData.enumerated().map({ Point(CGFloat($1), CGFloat($1 * step)) }), xStepValue: xStepValue, yStepValue: yStepValue)
//    }
//
//    init(yAxisData: [Double], xStepValue: CGFloat, yStepValue: CGFloat) throws {
//        if yAxisData.count < 2 {
//            throw ChartError.minTwoPointsDataRequired
//        }
//
//        let step = yAxisData.max()! / Double(yAxisData.count)
//        try self.init(points: yAxisData.enumerated().map({ Point(CGFloat($1 * step), CGFloat($1)) }), xStepValue: xStepValue, yStepValue: yStepValue)
//    }
//
//    init(xAxisData: [Double], xStepValue: CGFloat, yStepValue: CGFloat, yAxisData: [Double]) throws {
//        if xAxisData.count != yAxisData.count {
//            throw ChartError.mismatchXYaxisDataSize
//        }
//
//        if xAxisData.count < 2 {
//            throw ChartError.minTwoPointsDataRequired
//        }
//
//        let step = yAxisData.max()! / Double(yAxisData.count)
//        try self.init(points: yAxisData.enumerated().map({ Point(CGFloat($1 * step), CGFloat($1)) }), xStepValue: xStepValue, yStepValue: yStepValue)
//    }
    
    private var gridBody: some View {
        GeometryReader { geometry in
            Path { path in
                let xStepWidth = geometry.size.width / CGFloat(self.xStepsCount)
                let yStepWidth = geometry.size.height / CGFloat(self.yStepsCount)
                
                /// Y axis lines
                (0...self.yStepsCount).forEach { index in
                    let y = CGFloat(index) * yStepWidth
                    path.move(to: .init(x: 0, y: y))
                    path.addLine(to: .init(x: geometry.size.width, y: y))
                }
                
                /// X axis lines
                (0...self.xStepsCount).forEach { index in
                    let x = CGFloat(index) * xStepWidth
                    path.move(to: .init(x: x, y: 0))
                    path.addLine(to: .init(x: x, y: geometry.size.height))
                }
            }
            .stroke(Color.gray.opacity(0.5))
        }
    }
    
    /// straight style
    private var chartBodyForStarightStyle: some View {
        GeometryReader { geometry in
            Path { path in
                path.move(to: .init(x: 0, y: geometry.size.height))
                self.points.forEach { point in
                    
                    let x = (point.x / self.points.maxXValue) * geometry.size.width
                    let y = geometry.size.height - (point.y / self.points.maxYValue) * geometry.size.height
                    
                    path.addLine(to: .init(x: x, y: y))
                }
            }
            .stroke(
                Color.black,
                style: StrokeStyle(lineWidth: 3)
            )
        }
    }
    
    /// curved style
    private var chartBodyForCurvedStyle: some View {
        GeometryReader { geometry in
            VStack {
                Text("\(self.title)").font(.title2)
            }

            Path { path in
                path.move(to: .init(x: 0, y: geometry.size.height))

                var previousPoint = Point(0, geometry.size.height)
                path.addArc(center: previousPoint.cgpoint, radius: 4, startAngle: .degrees(0), endAngle: .degrees(360), clockwise: false)
                
                self.points.forEach { point in
                    let x = (point.x / self.points.maxXValue) * geometry.size.width
                    let y = geometry.size.height - (point.y / self.points.maxYValue) * geometry.size.height
                    
                    let deltaX = x - CGFloat(previousPoint.x)
                    let curveXOffset = deltaX * CGFloat(self.radius)
                    
                    let nextPoint = Point(x, y)
                    
                    path.addCurve(to: nextPoint.cgpoint,
                                  control1: .init(x: previousPoint.x + curveXOffset, y: previousPoint.y),
                                  control2: .init(x: x - curveXOffset, y: y ))
                    
                    path.addArc(center: nextPoint.cgpoint, radius: 3, startAngle: .degrees(0), endAngle: .degrees(360), clockwise: false)
                                        
                    path.move(to: nextPoint.cgpoint)

                    previousPoint = nextPoint
                }
            }
            .stroke(
                Color.red,
                style: StrokeStyle(lineWidth: 3)
            )
        }
        .padding()

    }

    var body: some View {
        ZStack {
            if self.girdsVisible {
                gridBody
            }
            
            if style == .straight {
                chartBodyForStarightStyle
            }
            else {
                chartBodyForCurvedStyle
            }
        }
    }
    
    func hiddenGrid(_ hidden: Bool) -> LineChart {
        var view = self
        view._girdsVisible = State(initialValue: !hidden)
        return view
    }
    
    func chartStyle(_ style: Style, radiusForCurvedStyle: Float = 0.5) -> LineChart {
        var view = self
        view._radius = State(initialValue: radiusForCurvedStyle)
        view._style = State(initialValue: .curved)
        return view
    }
}

struct LineChart_Preview: PreviewProvider {
    static var previews: some View {
        LineChart(title: "Chart View")
            .hiddenGrid(true)
            .chartStyle(.curved, radiusForCurvedStyle: 0.5)
            .frame(width: 300, height: 300, alignment: .center)
    }
}
