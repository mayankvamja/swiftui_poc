//
//  SignupPage.swift
//  iNews
//
//  Created by Mayank Vamja on 11/06/21.
//

import SwiftUI

struct SignupPage: View {
    var body: some View {
            
            VStack {
                NavigationLink("Already have an account? Login", destination: LoginPage())
//                    .navigationViewStyle(StackNavigationViewStyle())
                    .navigationBarTitle("Signup")
            }
    }
}

struct SignupPage_Previews: PreviewProvider {
    static var previews: some View {
        SignupPage()
    }
}
