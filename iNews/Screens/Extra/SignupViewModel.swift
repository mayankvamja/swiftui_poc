//
//  SignupViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 11/06/21.
//

import SwiftUI

class SignupViewModel: ObservableObject {
    
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var confirmPassword: String = ""
    
}
