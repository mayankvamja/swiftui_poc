//
//  HealthDashboardView.swift
//  iNews
//
//  Created by Mayank Vamja on 07/09/21.
//

import SwiftUI
import HealthKit

struct HealthDashboardView: View {
        
    @StateObject private var viewModel: HealthDashboardViewModel
    
    init() {
        _viewModel = StateObject(wrappedValue: HealthDashboardViewModel())
    }
    
    init(_ viewModel: HealthDashboardViewModel) {
        _viewModel = StateObject(wrappedValue: viewModel)
    }
    
    var body: some View {
        
        NavigationView {
            
            if viewModel.hasPermission == nil {
                VStack(alignment: .leading) {
                    CircularLoader(color: .blue)
                }
                .navigationTitle("Health & Fitness")
            } else if viewModel.hasPermission! {
                BarChart("", $viewModel.chartItems, shouldShowXAxisLabel: true)
                    .padding()
                    .frame(maxHeight: 500)
                    .navigationTitle("Health & Fitness")
            } else {
                VStack(alignment: .leading) {
                    Text("Access to health data required. Open settings and allow Steps count read access from health store.")
                    VStack {
                        Button("Open Settings") {
                            
                        }
                    }
                }
                .navigationTitle("Health & Fitness")
            }
            
        }
        .onAppear(perform: viewModel.onAppear)
        
    }
}

struct HealthDashboardView_Previews: PreviewProvider {
    static var previews: some View {
        HealthDashboardView()
    }
}
