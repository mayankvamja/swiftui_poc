//
//  HealthDashboardViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 07/09/21.
//

import SwiftUI
import HealthKit

class HealthDashboardViewModel: ObservableObject {
    
    private static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM"
        return formatter
    }()
    
    let healthStore: HealthStore
    var steps: [StepCount] = [StepCount]()
    
    @Published var hasPermission: Bool? = nil
    @Published var chartItems: [BarChart.Item] = []
    
    init() {
        healthStore = HealthStore()
    }
    
    init(_ store: HealthStore) {
        healthStore = store
    }
    
    func onAppear() {
        
        print(#function)
        
        healthStore.requestAuthorization { success in
            
            DispatchQueue.main.async {
                self.hasPermission = success
                self.steps.removeAll()
                self.chartItems.removeAll()
            }
            
            if success {
                self.healthStore.calculateSteps { statisticsCollection in
                    if let statisticsCollection = statisticsCollection {
                        // update the steps data
                        self.updateFromStatistics(statisticsCollection)
                    }
                }
            }
        }
    }
    
    private func updateFromStatistics(_ statisticsCollection: HKStatisticsCollection) {
        
        let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        let endDate = Date()
        
        statisticsCollection.enumerateStatistics(from: startDate, to: endDate) { (statistics, stop) in
            
            let count = statistics.sumQuantity()?.doubleValue(for: .count())
            
            let step = StepCount(count: Int(count ?? 0), date: statistics.startDate)
            
            DispatchQueue.main.async {
                self.steps.append(step)
                self.chartItems.append(.init(
                    Double(step.count),
                    label: Self.formatter.string(from: step.date),
                    color: step.count > 5000 ? Color.green :Color.red
                ))
            }
        }
        
    }
}
