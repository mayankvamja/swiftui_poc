//
//  Step.swift
//  iNews
//
//  Created by Mayank Vamja on 07/09/21.
//

import Foundation

struct StepCount: Identifiable {
    let id = UUID()
    let count: Int
    let date: Date
}
