//
//  LoginPage.swift
//  iNews
//
//  Created by Mayank Vamja on 10/06/21.
//

import SwiftUI

struct LoginPage: View {
    
    @StateObject var viewModel = LoginViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 20) {
                    Image("mobile_login")
                        .resizable()
                        .frame(maxWidth: .infinity)
                        .aspectRatio(contentMode: .fit)
                    
                    Text("Login")
                        .font(.system(size: 56, weight: .bold, design: .rounded))
                        .foregroundColor(.primaryColor)
                    
                    TextInput($viewModel.username, label: "Username", validation: viewModel.validateUsername(_:))
                    
                    TextInput($viewModel.password, fieldType: .secure, label: "Password", validation: viewModel.validatePassword(_:))
                    
                    Button("Login", action: { viewModel.login() })
                        .padding()
                        .frame(minWidth: 150)
                        .font(.system(size: 25, weight: .bold, design: .monospaced))
                        .foregroundColor(.white)
                        .background(Capsule().fill(Color.primaryColor))
                        .shadow(color: Color(0xa0a5c5), radius: 5, x: 0, y: 0)
                    
                    Spacer()
                    
                    NavigationLink("Don't have an account? Signup now", destination: SignupPage())
                    
                }
                .padding()
            }
            .navigationBarHidden(true)
            .actionSheet(isPresented: $viewModel.showError, content: {
                ActionSheet(title: Text("Failed to Login"), message: Text(viewModel.errorMsg), buttons: [
                    .cancel(Text("Okay"))
                ])
                
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .background(Color.backgroundColor.ignoresSafeArea())
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        LoginPage()
        
    }
}
