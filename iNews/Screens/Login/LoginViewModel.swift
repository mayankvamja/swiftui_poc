//
//  LoginViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 11/06/21.
//

import SwiftUI

class LoginViewModel: ObservableObject {
    
    @AppStorage("isLoggedIn") private var isLoggedIn: Bool = false
    
    @Published var username = ""
    @Published var password = ""
    @Published var errorMsg: String = ""
    @Published var showError: Bool = false

    private let validationService = ValidationService()
    
    func validateUsername(_ value: String) -> (valid: Bool, error: String?) {
        do {
            try validationService.validateUserName(text: value)
            return (valid: true, error: nil)
        }
        catch {
            return (valid: false, error: error.localizedDescription)
        }
    }
    
    func validatePassword(_ value: String) -> (valid: Bool, error: String?) {
        do {
            try validationService.validatePassword(text: value)
            return (valid: true, error: nil)
        }
        catch {
            return (valid: false, error: error.localizedDescription)
        }
    }
    
    func login() {
        var errorMsgs: [String?] = []
        let usernameValidation = validateUsername(username)
        let passwordValidation = validatePassword(password)
        
        errorMsgs.append(usernameValidation.error)
        errorMsgs.append(passwordValidation.error)
        
        if errorMsgs.compactMap({ $0 }).count == 0 {
            self.isLoggedIn = true
            self.errorMsg = ""
        }
        else {
            self.showError = true
            self.errorMsg = errorMsgs.compactMap({ $0 }).joined(separator: "\n")
        }
    }
    
}
