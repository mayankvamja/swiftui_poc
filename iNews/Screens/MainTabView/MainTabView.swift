//
//  ExpenseManager.swift
//  iNews
//
//  Created by Mayank Vamja on 14/06/21.
//

import SwiftUI

struct MainTabView: View {
    
    @State var currentTab: Int = 1
    private var context = PersistanceController.transactions.container.viewContext
    
    var body: some View {
        TabView(selection: $currentTab) {
            Dashboard(tab: $currentTab)
                .tag(1)
                .tabItem {
                    TabItem("Dashboard", sfSymbol: "coloncurrencysign.circle")
                }
            
            Expenses()
                .tag(2)
                .tabItem {
                    TabItem("List", sfSymbol: "list.bullet.below.rectangle")
                }
            
            NewsHomePage()
                .tag(3)
                .tabItem {
                    TabItem("More", sfSymbol: "ellipsis")
                }
            
            HealthDashboardView()
                .tag(4)
                .tabItem {
                    TabItem("Health", sfSymbol: "heart.fill")
                }
        }
        .environment(\.managedObjectContext, context)
        
    }
}

struct FloatingAddButton: View {
    var body: some View {
        VStack {
            Spacer()
            
            HStack {
                Spacer()
                NavigationLink(
                    destination: AddTransaction(),
                    label: {
                        Image(systemName: "plus")
                            .resizable()
                            .frame(width: 30.0, height: 30.0)
                    })
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.primaryColor).cornerRadius(30)
                    .shadow(color: Color.gray.opacity(0.4), radius: 10, x: 0, y: 0)
                
            }
        } // VStack: Floating button
        .padding(30)
    }
}

struct TabItem: View {
    
    let image: Image
    let label: String
    
    init(_ label: String = "", image: Image) {
        self.image = image
        self.label = label
    }
    
    init(_ label: String = "", sfSymbol: String) {
        self.image = Image(systemName: sfSymbol)
        self.label = label
    }
    
    var body: some View {
        VStack {
            image
                .resizable()
                .frame(maxWidth: 80, maxHeight: 80)
            
            Text(label)
        }
    }
}

struct ExpenseManager_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
