//
//  OnBoardingCard.swift
//  iNews
//
//  Created by Mayank Vamja on 24/05/21.
//

import SwiftUI

// TODO: fix scale animation
struct OnBoardingCard: View {
    
    @AppStorage("isOnBoarded") private(set) var onBoarded: Bool = false
    @State private var scale: CGFloat = 0.6
    
    private let isLastPage: Bool
    private let data: OnBoardingModel
    
    init(_ data: OnBoardingModel, isLastPage: Bool = false) {
        self.data = data
        self.isLastPage = isLastPage
    }
    
    var body: some View {
        ZStack {
            VStack(spacing: 20) {
                Image(data.image)
                    .resizable()
                    .scaledToFit()
                    .shadow(color: Color.black.opacity(0.3), radius: 20, x: 4, y: 4)
                
                Text(data.title)
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    .fontWeight(.bold)
                    .shadow(color: Color.black.opacity(0.3), radius: 8, x: 4, y: 4)
                
                Text(data.content)
                    .font(.body)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: 400)
                
                if isLastPage {

                    Button(action: {
                        onBoarded = true
                    }, label: {
                        HStack {
                            Text("Start")
                            Image(systemName: "arrow.right.circle")
                        }
                        .padding(.horizontal, 20)
                        .padding(.vertical, 10)
                        .background(Capsule().strokeBorder())
                    })
                    .accentColor(.white)
                }
                
            } // : VStack
            .frame(maxWidth: 600)
            .padding(20)
            .scaleEffect(scale)

        } // : ZStack
        .onDisappear {
//            withAnimation(.easeOut(duration: 0.5)) {
//                scale = 0.6
//            }
        }
        .onAppear {
            withAnimation(.easeOut(duration: 1.5)) {
                scale = 1.0
            }
        }
        .frame(minWidth: 0, maxWidth: 600, minHeight: 0, maxHeight: 900, alignment: .center)
        .background(LinearGradient(gradient: Gradient(colors: data.colors), startPoint: .topLeading, endPoint: .bottomTrailing))
        .cornerRadius(25)
        .padding()
        .shadow(color: data.colors[0].opacity(0.3), radius: 8, x: 0, y: 0)
    }
}

struct OnBoardingCard_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingCard(OnBoardingModel(title: "Title", content: "Content lorem ipsum lorem ipsum lorem ipsum", image: "newspaper", colors: [.blue, .purple]))
    }
}
