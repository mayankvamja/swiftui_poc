//
//  OnBoardingPage.swift
//  iNews
//
//  Created by Mayank Vamja on 24/05/21.
//

import SwiftUI

struct OnBoardingPage: View {
    
    @State var currentPage: Int = 0
    
    // MARK: Properties
    let content: [OnBoardingModel] = [
        OnBoardingModel(title: "Latest News", content: "Get news latest, trending from tons of different and trustworthy sources.", image: "newspaper", colors: [Color(28, 216, 210), Color(147, 237, 199)]),
        OnBoardingModel(title: "See Trending", content: "See what's trending online. See recently trending, last week or last month trending news articles.", image: "modern_design", colors: [Color(92, 37, 141), Color(67, 137, 162)]),
        OnBoardingModel(title: "Vast Categories", content: "Read news by number of categories.", image: "categories", colors: [Color(235, 51, 73), Color(244, 92, 67)]),
        OnBoardingModel(title: "Full Coverage", content: "Read news directly in app and see original news article in app itself without any hustle.", image: "read_articles", colors: [Color(170, 7, 107), Color(97, 4, 95)]),
        OnBoardingModel(title: "Search Easily", content: "Search for news articles and get search suggesstions as you type.", image: "search_news", colors: [Color(116, 116, 191), Color(52, 138, 199)])
    ]
    
    init() {
//        _tab = State(initialValue: content.first?.title)
    }
    
    // MARK: body
    
    var body: some View {
        VStack {
            TabView(selection: $currentPage) {
                
                if content.count > 0, let last = content.last {
                    OnBoardingCard(last, isLastPage: true)
                        .tag(-1)
                }

                ForEach(content.indices) { index in
                    OnBoardingCard(content[index], isLastPage: index == content.count-1).tag(index)
                }
                
                if content.count > 0, let first = content.first {
                    OnBoardingCard(first, isLastPage: false)
                        .tag(content.count)
                }

            }
            .onChange(of: currentPage, perform: { value in
                if value == -1 {
                    currentPage = content.count-1
                }
                else if value == content.count {
                    currentPage = 0
                }
            })
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
//            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            
            HStack {
                ForEach(content.indices) { i in
                    Image(systemName: currentPage == i ? "circle.fill" : "circle")
                        .foregroundColor(content[i].colors[0])
                        .onTapGesture {
                            withAnimation(.easeIn(duration: 0.8)) {
                                currentPage = i
                            }
                        }
                }
            }
        }
    }
}

// MARK: Preview
struct OnBoardingPage_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingPage()
    }
}
