//
//  NewsDetail.swift
//  iNews
//
//  Created by Mayank Vamja on 22/06/21.
//

import SwiftUI

struct NewsDetail: View {
    
    @State var url: URL?
    let title: String
    
    init(_ artical: NewsArticle) {
        if let urlString = artical.url {
            self._url = State(initialValue: URL(string: urlString))
        }
        else {
            self._url = State(initialValue: nil)
        }
        self.title = artical.title ?? "News page"
    }
    
    var body: some View {
        VStack {
            Text(title).lineLimit(1).padding(.horizontal)
                .padding(.vertical, 8)
            WebView($url)
        }.navigationBarTitle("", displayMode: .inline)
    }
}
