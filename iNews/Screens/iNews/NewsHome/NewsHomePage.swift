//
//  NewsHomePage.swift
//  iNews
//
//  Created by Mayank Vamja on 24/05/21.
//

import SwiftUI

struct NewsHomePage: View {
    
    @StateObject private var viewModel = NewsHomeViewModel()
    
    var body: some View {
        NavigationView {
            
            VStack {
                
                if viewModel.loading {
                    ScrollView {
                        LoadingNewsHomePlaceholder()
                    }.padding()
                }
                else if let errorMessage = viewModel.error {
                    Spacer()
                    Text("An error occured!").font(.title)
                    Text(errorMessage).font(.subheadline)
                    Button("Refresh", action: viewModel.fetchTrendingNews)
                        .padding()
                    Spacer()
                }
                else {
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(spacing: 20) {
                            ForEach(viewModel.tendingTopics?.topics ?? []) { topic in
                                TopicButton(topic.query ?? "Unknown", viewModel.selectedTopic == topic) {
                                    viewModel.selectedTopic = topic
                                }
                            }
                        }.padding(8)
                    }
                    
                    ScrollView {
                        ScrollViewReader { reader in
                            EmptyView().id("LIST_TOP")
                            ForEach(viewModel.selectedTopic?.articles ?? [], id: \.id) { article in
                                
                                // For default Safari WebView
//                                if let stringUrl = article.url,
//                                   let url = URL(string: stringUrl) {
//                                    Link(destination: url) {
//                                        NewsCard(article)
//                                    }
//                                    .padding(8)
//                                }
                                
                                // For WKWebView
                                NavigationLink(
                                    destination: NewsDetail(article),
                                    tag: article.id,
                                    selection: $viewModel.newsSelection,
                                    label: {
                                        NewsCard(article)
                                    }).padding(8)
                                
                            }
                            .onChange(of: viewModel.selectedTopic, perform: { _ in
                                withAnimation(.easeInOut(duration: 2)) {
                                    reader.scrollTo("LIST_TOP", anchor: .top)
                                }
                            })
                        }
                    }
                    
                }
                
            }
            .padding(8)
            .navigationBarItems(trailing: NavigationLink(
                                    destination: NewsSearch(),
                                    label: {Label("Search", systemImage: "magnifyingglass.circle.fill")
                                    }))
            .navigationBarTitle("iNews", displayMode: .inline)
        }
        .onAppear(perform: {
            viewModel.fetchTrendingNews()
        })
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct LoadingNewsHomePlaceholder: View {
    var body: some View {
        ForEach(0 ..< 10) { _ in
            LazyListItem()
                .padding(.top, 10)
                .padding(.bottom, 10)
        }
    }
}

struct NewsHomePage_Previews: PreviewProvider {
    static var previews: some View {
        return
            VStack {
                Group {
                    NewsCard(NewsArticle(title: "News title", content: "new cpns das da slkd la sd ka sd as dl als dklasnljdn  ", source: "kasdas", timeAgo: "few min ago", url: "https://picsum.photo/500", imageURL: "https://picsum.photos/500"))
                }.padding(.bottom, 15)
                Group {
                    NewsCard(NewsArticle(title: "News title", content: "new cpns das da slkd la sd ka sd as dl als dklasnljdn  ", source: "kasdas", timeAgo: "few min ago", url: "https://picsum.photo/500", imageURL: "https://picsum.photos/500"))
                }.padding(.bottom, 30)
            }
    }
}
