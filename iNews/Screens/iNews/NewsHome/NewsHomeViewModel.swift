//
//  NewsHomeViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 01/06/21.
//

import Foundation
import SwiftUI
import Combine

class NewsHomeViewModel: ObservableObject {
    
    @Published private(set) var loading = false
    @Published private(set) var tendingTopics: TrendingNewsTopics? = nil
    @Published var selectedTopic: TrendingTopic? = nil
    @Published var newsSelection: UUID? = nil
    
    @Published private(set) var articles: [NewsArticle] = []
    private var page: Int = 1
    @Published private(set) var isLastPage: Bool = false
    
    @Published var error: String? = nil
    
    private var tokens = Set<AnyCancellable>()
    private let manager: HTTPManagerProtocol
    
    init(_ manager: HTTPManagerProtocol = HTTPManager.shared) {
        self.manager = manager
        
        HTTPRequest.defaultHost = Endpoints.NewsAPI.host
        HTTPRequest.defaultHeaders = ["app-id": "c5UCry4pCDpO1icFxjwS"]
    }
    
    deinit {
        HTTPRequest.defaultHost = ""
        HTTPRequest.defaultHeaders = nil
    }
    
    func fetchTrendingNews() {
        error = nil
        loading = true
                
        manager.request(HTTPRequest(Endpoints.NewsAPI.trendingTopics))
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] completion in
                    self?.apiCompletionHandler(completion)
                }, receiveValue: { [weak self] (value: TrendingNewsTopics) in
                    self?.tendingTopics = value
                    if value.topics.count > 0 {
                        self?.selectedTopic = value.topics[0]
                    }
                }
            ).store(in: &tokens)
        
    }
    
    func fetchNews(by category: NewsCategory, reset: Bool = false) {
        if reset {
            page = 1
            isLastPage = false
        }
        else if isLastPage {
            return
        }
        
        loading = true
        let req: HTTPRequest = HTTPRequest(Endpoints.NewsAPI.newsByCategory(category), parameters: ["page": "\(page)"])
        
        manager.request(req)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] completion in
                    self?.apiCompletionHandler(completion)
                },
                receiveValue: { [weak self] (value: NewsData) in
                    self?.articles += value.articles
                    if Float(value.totalResults) / Float(value.pagesize) > Float(value.page) {
                        self?.page = value.page + 1
                    }
                    else {
                        self?.isLastPage = true
                    }
                }
            ).store(in: &tokens)
        
    }
    
    func apiCompletionHandler<E>(_ completion: Subscribers.Completion<E>) {
        self.loading = false
        
        switch completion {
        case .failure(let err):
            
            if let _err = err as? HTTPError {
                self.error = _err.errorDescription
            }
            else {
                self.error = "err.localizedDescription"
            }
        case .finished:
            self.error = nil
        }
    }
}
