//
//  NewsCard.swift
//  iNews
//
//  Created by Mayank Vamja on 01/06/21.
//

import Foundation
import SwiftUI

struct NewsCard: View {
    
    static let colors: [Color] = [
        Color(0x2a9d8f),
        Color(0xe63946),
        Color(0xef476f),
        Color(0x3a86ff),
        Color(0x0081a7)
    ]
    
    let title: String
    let source: String
    let content: String
    let datetime: String
    let imageUrl: String
    let randomColor = NewsCard.colors[Int.random(in: 0...4)]
    
    init(_ article: NewsArticle) {
        self.source = article.source ?? ""
        self.title = article.title ?? "- -"
        self.content = article.content ?? ""
        self.datetime = article.timeAgo ?? ""
        self.imageUrl = article.imageURL ?? ""
    }
    
    var body: some View {
        HStack(spacing: 20) {
            URLImage(url: URL(string: imageUrl)) {
                Image(systemName: "newspaper.fill")
            }
            .aspectRatio(contentMode: .fill)
            .frame(width: 100, height: 100)
            .cornerRadius(20)
            
            VStack(alignment: .leading, content: {
                Text(source)
                    .foregroundColor(randomColor)
                    .padding(.top, 2)
                    .padding(.leading, 10)
                    .padding(.bottom, 2)
                    .padding(.trailing, 10)
                    .background(Capsule().stroke(randomColor))
                    .font(.callout)
                    .lineLimit(2)
                
                Text(title)
                    .foregroundColor(Color(0x212121))
                    .font(.title3)
                    .lineLimit(2)
                
                Text(content)
                    .foregroundColor(Color(0x313131))
                    .font(.body)
                    .lineLimit(2)
                
                HStack {
                    Spacer()
                    Text(datetime)
                        .foregroundColor(Color(0x414141))
                        .font(.caption)
                }
                
            })
            .frame(maxWidth: 400, alignment: .topLeading)
            
        }
        .padding(.all, 10)
        .background(Color.white)
        .cornerRadius(20)
        .shadow(color: Color.gray.opacity(0.2), radius: 8, x: 0, y: 0)
    }
}
