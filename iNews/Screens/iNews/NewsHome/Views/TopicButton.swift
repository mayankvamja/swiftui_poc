//
//  TopicButton.swift
//  iNews
//
//  Created by Mayank Vamja on 01/06/21.
//

import Foundation
import SwiftUI

struct TopicButton: View {
    private let selected: Bool
    private let text: String
    private var onSelect: () -> Void
    
    init(_ text: String, _ selected: Bool = true, _ action: @escaping () -> Void) {
        self.text = text
        self.selected = selected
        self.onSelect = action
    }
    
    var body: some View {
        Button(action: {
            if !selected {
                onSelect()
            }
        }, label: {
            Text(text)
                .font(.title2)
                .fontWeight(.bold)
        })
        .padding()
        .foregroundColor(selected ? .white : Color(0x212121))
        .background(LinearGradient(gradient: Gradient(colors: selected ? [Color(0x00c6ff), Color(0x0072ff)]: [Color.white]), startPoint: .topLeading, endPoint: .bottomTrailing))
        .cornerRadius(16)
        .shadow(color: Color.gray.opacity(0.3), radius: 4, x: 0, y: 0)
    }
}
