//
//  NewsSearch.swift
//  iNews
//
//  Created by Mayank Vamja on 23/06/21.
//

import SwiftUI

// : Fix Error:
// === AttributeGraph: cycle detected through attribute 00000 ===
struct NewsSearch: View {
    
    @StateObject var viewModel = NewsSearchViewModel()
    @State private var animateDropDown: Bool = true
    @State private var isSearchBarFocused: Bool? = false
    
    var body: some View {
        ZStack {
            
            Color.backgroundColor.ignoresSafeArea()
                .gesture(
                    TapGesture()
                        .onEnded(viewModel.resignFirstResponder)
                )
            
            VStack {
                CustomTextField(text: $viewModel.query, placeholder: "Search here", isResponder: $isSearchBarFocused)
                    .padding(.horizontal)
                    .frame(maxHeight: 50)
                    .background(Capsule().stroke())
                
                ZStack(alignment: .top) {
                    
                    if viewModel.query.trimmingCharacters(in: .whitespaces).count > 0 {
                        List {
                            
                            Button(action: {
                                print("KeywordListItemView.onTapGesture")
                                viewModel.searchNews(viewModel.query)
                            }, label: {
                                KeywordListItemView(item: TopicKeyword(mid: "", title: "Search for \"\(viewModel.query)\"", type: "search"))
                            })
                            
                            ForEach(viewModel.searchResults, id: \.self) { keyword in
                                KeywordListItemView(item: keyword)
                                    .onTapGesture {
                                        print("KeywordListItemView.onTapGesture")
                                        viewModel.searchNews(keyword.title)
                                    }
                            }
                        }
                        .cornerRadius(20)
                        .shadow(color: .shadowColor, radius: 10, x: 0, y: 0)
                        .frame(maxHeight: animateDropDown ? viewModel.dropDownHeight : 0)
                        .zIndex(1)
                        
                    }
                    
                    ScrollView {
                        LazyVStack {
                            
                            if let results = viewModel.newsData?.totalResults {
                                Text("Found \(results) results for \"\(viewModel.query)\"")
                                    .font(.callout)
                                    .italic()
                                    .foregroundColor(.gray)
                                    .padding(.vertical)
                            }
                            
                            ForEach(viewModel.articles, id: \.self) { article in
                                NavigationLink(
                                    destination: NewsDetail(article),
                                    tag: article.id,
                                    selection: $viewModel.newsSelection,
                                    label: {
                                        NewsCard(article)
                                    }).padding(8)
                                    .onAppear(perform: {viewModel.loadMoreNews(article.id)})
                            }
                            
                            if viewModel.fetchingNews {
                                Spinner(.primaryColor, .contrastColor, .secondaryColor)
                                    .padding()
                                    .frame(width: 80, height: 80)
                            }
                        }
                    }

                }
                
                Spacer()
            }
            .onChange(of: isSearchBarFocused, perform: { value in
                withAnimation(.easeInOut(duration: 0.3)) {
                    animateDropDown = value == true
                }
            })
            
            .onAppear(perform: {
                isSearchBarFocused = true
            })
            
            .onDisappear(perform: {
                viewModel.resignFirstResponder()
            })
            
            .padding()
            .navigationBarTitle("Search", displayMode: .inline)
        }
    }
}

struct KeywordListItemView: View {
    let item: TopicKeyword
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(item.title)
                    .font(.subheadline)
                    .foregroundColor(.primaryColor)
                Text(item.type)
                    .font(.caption)
                    .foregroundColor(.gray)
            }
            Spacer()
            Image(systemName: "line.diagonal.arrow")
                .foregroundColor(.primaryColor)
        }
        .padding(8)
        
    }
}

struct NewsSearch_Previews: PreviewProvider {
    static var previews: some View {
        NewsSearch()
    }
}
