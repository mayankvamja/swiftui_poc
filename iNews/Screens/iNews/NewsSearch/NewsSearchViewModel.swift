//
//  NewsSearchViewModel.swift
//  iNews
//
//  Created by Mayank Vamja on 23/06/21.
//

import Foundation
import CoreGraphics
import Combine
import UIKit.UIApplication

class NewsSearchViewModel: ObservableObject {
    
    var newsData: NewsData? = nil
    @Published var newsSelection: UUID? = nil
    @Published var articles: [NewsArticle] = []
    @Published var fetchingNews: Bool = false
    @Published var searchResults: [TopicKeyword] = []
    @Published var query: String = ""
    @Published var searchFocused: Bool? = false
    
    @Published var error: (hasError: Bool, message: String)? = nil
    
    var tokens = Set<AnyCancellable>()
    private let manager: HTTPManagerProtocol
    
    var dropDownHeight: CGFloat {
        min(400, CGFloat(60 * (searchResults.count + 1)))
    }
    
    var isLastPage: Bool {
        Double((newsData?.totalResults ?? 0)) / Double(max(articles.count, 1)) <= 0
    }
    
    init(_ manager: HTTPManagerProtocol = HTTPManager.shared) {
        self.manager = manager
        
        HTTPRequest.defaultHost = Endpoints.NewsAPI.host
        HTTPRequest.defaultHeaders = ["app-id": "c5UCry4pCDpO1icFxjwS"]
        
        $query.debounce(for: .seconds(1), scheduler: DispatchQueue.main)
            .map({$0.trimmingCharacters(in: .whitespaces)})
            .sink(receiveValue: searchKeywords(with:))
            .store(in: &tokens)
    }
    
    deinit {
        HTTPRequest.defaultHost = ""
        HTTPRequest.defaultHeaders = nil
    }
    
    func resignFirstResponder() {
        UIApplication.shared.windows.first {$0.isKeyWindow }?.endEditing(true)
    }
    
    func searchKeywords(with query: String) {
        guard query.count > 1 else {
            return
        }
        
        let request = HTTPRequest(Endpoints.NewsAPI.topicsSearch, parameters: ["q": query])
        manager.request(request)
            .receive(on: DispatchQueue.main)
            .sink { (completion) in
                switch completion {
                case .finished:
                    self.error = nil
                case .failure(let err):
                    self.error = (hasError:true, message:err.localizedDescription)
                }
            } receiveValue: { (results) in
                self.searchResults = results
            }
            .store(in: &tokens)
        
    }
    
    func loadMoreNews(_ id: UUID) {
        guard !isLastPage else {
            print("Last page")
            return
        }
        guard articles.last?.id == id else {
            print("Not list-last item")
            return
        }
        searchNews(query)
    }
    
    func searchNews(_ text: String) {
        fetchingNews = true
        query = text
        
        resignFirstResponder()
        
        let page = (newsData?.page ?? 0) + 1
        let request = HTTPRequest(Endpoints.NewsAPI.searchNews, parameters: ["q": text, "page": "\(page)"])
        manager.request(request)
//            .delay(for: .seconds(5), scheduler: DispatchQueue.main)
            .receive(on: DispatchQueue.main)
            .sink { (completion) in
                self.fetchingNews = false
                switch completion {
                case .finished:
                    self.error = nil
                case .failure(let err):
                    self.error = (hasError:true, message:err.localizedDescription)
                }
            } receiveValue: { (result: NewsData) in
                self.newsData = result
                self.articles += result.articles
            }
            .store(in: &tokens)
    }
}
