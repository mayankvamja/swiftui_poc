//
//  URLSession+Extension.swift
//  iNews
//
//  Created by Mayank Vamja on 08/05/21.
//

import Foundation

// Auto generated methods
// MARK: - URLSession response handlers

extension URLSession {
    func codableTask<T: Decodable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            completionHandler(try? JSONDecoder().decode(T.self, from: data), response, nil)
        }
    }

    func getTrendingNewsTopics(with url: URL, completionHandler: @escaping (TrendingNewsTopics?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
    func getNews(with url: URL, completionHandler: @escaping (NewsData?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
    
}
