//
//  ValidationService.swift
//  POC
//
//  Created by Mayank Vamja on 21/04/21.
//

import Foundation
import UIKit

enum ValidationError: LocalizedError, CaseIterable {
    case requiredUsername
    case tooShortUsername
    case tooLongUsername
    
    case invalidEmail
    case emailRequired
    
    case passwordRequired
    case shortPassword
    case invalidPassword
    
    case confirmPasswordRequired
    case mismatchPaassword
    
    case phoneNumberRequired
    case phoneNumberShort
    case invalidPhoneNumber
    
    case invalidCredentials
    case emailRegistered
    
    case requiredDOB
    case invalidDOB
    
    var errorDescription: String? {
        switch self {
        case .requiredUsername:
            return "Username is required".localized
        
        case .tooShortUsername:
            return "Username is too short".localized

        case .tooLongUsername:
            return "Username must not exceed 20 characters".localized
        
        case .invalidEmail:
            return "Please enter valid email address".localized
        case .emailRequired:
            return "Email is required".localized
        
        case .passwordRequired:
            return "Password is required".localized
        case .shortPassword:
            return "Password is too short".localized
        case .invalidPassword:
            return "Password is invalid".localized
        
        case .confirmPasswordRequired:
            return "Please Confirm password".localized
        case .mismatchPaassword:
            return "Both password does not match".localized
        
        case .phoneNumberRequired:
            return "Phone number is required".localized
        case .phoneNumberShort:
            return "Phone number should be atleasr 10 characters".localized
        case .invalidPhoneNumber:
            return "Invalid phone number".localized
        
        case .invalidCredentials:
            return "Found Wrong credentials. Try again...".localized
        case .emailRegistered:
            return "Email is already registered".localized
        
        case .requiredDOB:
            return "Date of birth is required".localized
        case .invalidDOB:
            return "Please select valid date of birth".localized
        }
    }
}

enum PasswordStrength: String {
    case tooWeak = "Much Weaker"
    case weak = "Weak"
    case strong = "Strong"
    case tooStrong = "Much Stronger"
    
    var color: UIColor {
        switch self {
        case .tooWeak:
            return .red
        case .weak:
            return .orange
        case .strong:
            return .systemTeal
        case .tooStrong:
            return .systemGreen
        }
    }
    
    var label: String { return self.rawValue.localized }
}

struct ValidationService {
    
    private typealias VE = ValidationError
    
    private let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}"
        + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
        + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
        + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
        + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
        + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
        + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    
    // MARK: Validation Methods
    
    func validateUserName(text: String?) throws {
    
        guard let text = text?.trimmingCharacters(in: .whitespacesAndNewlines) else { throw VE.requiredUsername }
        guard text.count > 0 else { throw VE.requiredUsername }
        guard text.count > 2 else { throw VE.tooShortUsername }
        guard text.count <= 20 else { throw VE.tooLongUsername }
        
    }
    
    func validatePhoneNumber(text: String?) throws {
    
        guard let text = text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        guard text.count > 0 else { return }
        guard isValid(input: text, regEx: "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$") else { throw VE.invalidPhoneNumber }
        guard text.count >= 10 else { throw VE.phoneNumberShort }
        
    }
    
    func validateEmail(text: String?) throws {
    
        guard let text = text else { throw VE.emailRequired }
        guard text.count > 0 else { throw VE.emailRequired }
        
        guard isValid(input: text, regEx: emailRegEx, predicateFormat: "SELF MATCHES[c] %@") else { throw VE.invalidEmail }
        
    }
    
    func validatePassword(text: String?) throws {
        
        guard let text = text else { throw VE.passwordRequired }
        guard text.count > 0 else { throw VE.passwordRequired }
        guard text.count >= 6 else { throw VE.shortPassword }
        
    }
    
    func validateNewPassword(text: String?) throws {
        
        guard let text = text else { throw VE.passwordRequired }
        guard text.count > 0 else { throw VE.passwordRequired }
        guard text.count >= 6 else { throw VE.shortPassword }
        
    }
    
    func validateConfirmPassword(text: String?, password: String?) throws {
        
        guard let text = text else { throw VE.confirmPasswordRequired }
        guard text.count > 0 else { throw VE.confirmPasswordRequired }
        guard text == password else { throw VE.mismatchPaassword }
        
    }
    
    func checkPasswordStrength(text: String) -> PasswordStrength {
        
        let tooStrong = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{8,}"
        let strong = "^(?=.*[a-z])(?=.*[A-Z])((?=.*\\d)||(?=.*[d$@$!%*?&#]))[A-Za-z\\dd$@$!%*?&#]{6,}"
        let weak = "^(?=.*[a-zA-Z])(?=.*\\d)[A-Za-z\\dd$@$!%*?&#]{6,}"
        
        return isValid(input: text, regEx: tooStrong)
            ? .tooStrong
            : isValid(input: text, regEx: strong)
            ? .strong
            : isValid(input: text, regEx: weak)
            ? .weak
            : .tooWeak
    }
    
    // MARK: private methods
    
    private func isValid(input: String, regEx: String, predicateFormat: String = "SELF MATCHES %@") -> Bool {
        return NSPredicate(format: predicateFormat, regEx).evaluate(with: input)
    }
}
