//
//  ViewModifiers.swift
//  iNews
//
//  Created by Mayank Vamja on 16/06/21.
//

import SwiftUI

/// To Show a clear button at right side of `TextField`
struct ClearButton: ViewModifier {
    @Binding var text: String
    
    public func body(content: Content) -> some View {
        ZStack(alignment: .trailing) {
            content
            
            if !text.isEmpty {
                Button(action: { self.text = "" }) {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(Color(UIColor.opaqueSeparator))
                }
                .padding(.trailing, 8)
            }
        }
    }
}
