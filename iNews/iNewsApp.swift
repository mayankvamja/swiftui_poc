//
//  iNewsApp.swift
//  iNews
//
//  Created by Mayank Vamja on 24/05/21.
//

import SwiftUI

/*class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("-- App didFinishLaunchingWithOptions --")
        return true
    }
}*/

@main
struct iNewsApp: App {
    
    // @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate

    @AppStorage("isOnBoarded") private var onBoarded: Bool = false
    @AppStorage("isLoggedIn") private var isLoggedIn: Bool = false
    
    @Environment(\.scenePhase) var scenePhase

    var body: some Scene {
        WindowGroup {
            if onBoarded {
                MainTabView()
                /*if isLoggedIn {
                    ExpenseManager()
                } else {
                    LoginPage()
                }*/
            } else {
                OnBoardingPage()
            }
        }
        .onChange(of: scenePhase) { (phase) in
            switch phase {
            case .background:
                print("-- scenePhase: APP IS IN BACKGROUND --> \(phase)")
            case .inactive:
                print("-- scenePhase: APP INACTIVATED      --> \(phase)")
            case .active:
                print("-- scenePhase: APP ACTIVATED        --> \(phase)")
            @unknown default:
                print("-- scenePhase: UNKNOWN PHASE        --> \(phase)")
            }
        }
    }
}
