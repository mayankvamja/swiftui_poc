//
//  TransactionsCoreDataTests.swift
//  iNewsTests
//
//  Created by Mayank Vamja on 23/06/21.
//

import XCTest
@testable import iNews
import CoreData.NSManagedObjectContext

class TransactionsCoreDataTests: XCTestCase {
    
    lazy var mocContext: NSManagedObjectContext = {
        let controller = MockPersistanceController.shared
        return controller.context
    }()
    
    var viewModel: TransactionViewModel!
    
    override func setUpWithError() throws {
        viewModel = TransactionViewModel()
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    // MARK: helper functions
    
    func createValidTransaction() {
        viewModel.title = "Transaction title"
        viewModel.amount = "1.0"
    }
    
    func createInvalidTransaction() {
        viewModel.title = "      "
        viewModel.amount = "0"
    }
    
    // MARK: Start: TransactionViewModel Tests
    
    func testTransactionValidation() throws {
        createValidTransaction()
        let validResult = viewModel.validate()
        
        XCTAssertTrue(validResult, "TransactionViewModel validate returned wrong result")
        
        createInvalidTransaction()
        let invalidResult = viewModel.validate()
        
        XCTAssertFalse(invalidResult, "TransactionViewModel validate returned wrong result")
    }
    
    func testAddNewTransaction_withValid_and_withInValid() throws {
        createInvalidTransaction()
        let expectation2 = XCTestExpectation()
        
        viewModel.save(in: mocContext) { (success) in
            XCTAssertFalse(success)
            expectation2.fulfill()
        }
        
        createValidTransaction()
        let expectation1 = XCTestExpectation()
        
        viewModel.save(in: mocContext) { (success) in
            XCTAssertTrue(success)
            expectation1.fulfill()
        }
        
        wait(for: [expectation1, expectation2], timeout: 5)
    }
    
    func testEditTransaction() throws {
        let transactionToEdit = Transactions(context: mocContext)
        transactionToEdit.title = "Title"
        transactionToEdit.amount = 100.0
        
        viewModel = TransactionViewModel(transaction: transactionToEdit)
        createValidTransaction()
        let expectation1 = XCTestExpectation()
        
        viewModel.save(in: mocContext) { (success) in
            XCTAssertTrue(success)
            
            let newTitle = "Transaction title"
            let newAmount = 1.0
            
            XCTAssertEqual(transactionToEdit.title, newTitle)
            XCTAssertEqual(transactionToEdit.amount, newAmount)
            
            expectation1.fulfill()
        }
        
        wait(for: [expectation1], timeout: 5)
    }
    
    func testAmountChangeHandler() throws {
        viewModel.amount = "100"
        viewModel.handleAmountChange("100")
        
        XCTAssertEqual(viewModel.amount, "100")

        viewModel.amount = "100a"
        viewModel.handleAmountChange("100a")
        XCTAssertEqual(viewModel.amount, "100")
        
        viewModel.amount = "abcd"
        viewModel.handleAmountChange("abcd")
        XCTAssertEqual(viewModel.amount, "")
    }
    
    func testRemoveImage() throws {
        viewModel.image = UIImage(systemName: "info")
        viewModel.removeImage()
        XCTAssertNil(viewModel.image)
    }
    
    // MARK: End: TransactionViewModel Tests

}
