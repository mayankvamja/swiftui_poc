//
//  MockPersistanceController.swift
//  iNewsTests
//
//  Created by Mayank Vamja on 23/06/21.
//

import Foundation
import CoreData

struct MockPersistanceController {
    static let shared = MockPersistanceController()
    
    let container: NSPersistentContainer
    
    var context: NSManagedObjectContext {
        container.viewContext
    }
    
    init() {
        container = NSPersistentContainer(name: "transactions")
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            if let _error = error {
                fatalError("Failed to load \(description.description) with error: \(_error)")
            }
        }
    }
}
